<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailSchedulersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_schedulers', function (Blueprint $table) {
            $table->id();
            $table->string('from_');
            $table->string('category_id')->nullable();
            $table->string('customer_type_id')->nullable();
            $table->string('subject');
            $table->text('description');
            $table->string('frequency');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->string('day')->nullable();;
            $table->string('time');
            $table->integer('delivered')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_schedulers');
    }
}
