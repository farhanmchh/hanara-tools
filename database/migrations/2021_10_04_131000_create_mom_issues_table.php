<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMomIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mom_issues', function (Blueprint $table) {
            $table->id();
            $table->integer('mom_id');
            $table->text('description');
            $table->enum('type', ['info', 'to do', 'tba', 'tbc', 'tbd']);
            $table->string('assigned');
            $table->string('due_date');
            $table->string('status');
            $table->string('date_resolved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mom_issues');
    }
}
