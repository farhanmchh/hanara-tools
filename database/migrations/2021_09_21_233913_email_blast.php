<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmailBlast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_blast', function (Blueprint $table) {
            $table->id();
            $table->text('from_');
            $table->integer('category_id')->nullable();
            $table->integer('customer_type_id')->nullable();
            $table->string('subject');
            $table->text('description');
            $table->integer('delivered');
            $table->dateTime('blast_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_blast');
    }
}
