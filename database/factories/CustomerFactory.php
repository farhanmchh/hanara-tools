<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Customer;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'phone' => $this->faker->phoneNumber(),
            'email' => $this->faker->companyEmail(),
            'address' => $this->faker->address(),
            'province' => $this->faker->state(),
            'city' => $this->faker->city(),
            'category_id' => $this->faker->numberBetween(1, 5),
            'customer_type_id' => $this->faker->numberBetween(1, 5),
            'pic_name' => $this->faker->name()
        ];
    }
}
