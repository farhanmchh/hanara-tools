<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Domain;

class DomainFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Domain::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->domainName(),
            'customer_id' => $this->faker->numberBetween(1, 5),
            'hosting' => $this->faker->numberBetween(0, 1),
            'cpanel' => $this->faker->numberBetween(0, 1),
            'user_cpanel' => $this->faker->userName(),
            'password_cpanel' => $this->faker->password(),
            'vendor_id' => $this->faker->numberBetween(1, 5),
            'expired' => $this->faker->creditCardExpirationDateString()
        ];
    }
}
