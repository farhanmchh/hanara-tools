<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Customer;
use App\Models\CustomerType;
use App\Models\Domain;
use App\Models\Item;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Vendor;
use App\Models\Category;
use App\Models\SubCountry;
use App\Models\TaskStatus;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    User::create([
      'name' => 'hanara',
      'email' => 'hanara@hanara.id',
      'password' => bcrypt('hanara')
    ]);

    Company::create([
      'name' => 'Hanara Sentra Teknologi',
      'pic' => 'Abdul Hamied',
      'role' => 'Account Executive',
      'phone' => '(021) 8371 4302',
      'email' => 'info@hanara.id',
      'address' => 'Unit 106, Bellezza BSA, Jl. Permata Hijau, Kebayoran Lama, Jakarta Selatan 12210',
      'website' => 'www.hanara.id',
      'logo' => 'hanara_logo.png',
      'tnc' => 'Tes'
    ]);

    CustomerType::insert(
      [
          [
              'name' => "Customer"
            
          ],
          [
              'name' => "Database Only"
              
          ],
          [
              'name' => "One Time Customer"
              
          ]
      ]
  );



    Item::factory(5)->create();

    Category::factory(5)->create();

    Customer::factory(5)->create();

    Vendor::factory(5)->create();

    Domain::factory(5)->create();


    $this->call([SubCountriesSeeder::class]);

  }
}
