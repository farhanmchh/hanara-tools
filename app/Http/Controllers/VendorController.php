<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.vendor.index', [
            'title' => 'Vendors',
            'vendors' => Vendor::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.vendor.create', [
            'title' => 'Add Vendor'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required'],
            'phone' => ['required', 'numeric'],
            'email' => ['required', 'email'],
            'address' => ['required'],
            'province' => ['required'],
            'city' => ['required'],
            'website' => [],
            'pic_email' => []
        ]);
        Vendor::create($validated);
        return redirect('/vendor')->with('success', 'Vendor has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('dashboard.vendor.show', [
            'title' => 'Detail Vendor',
            'vendor' => Vendor::where('id', $id)->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.vendor.edit', [
            'title' => 'Edit Vendor',
            'vendor' => Vendor::where('id', $id)->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => ['required'],
            'phone' => ['required', 'numeric'],
            'email' => ['required', 'email'],
            'address' => ['required'],
            'province' => ['required'],
            'city' => ['required']
        ]);

        $validated['website'] = $request->website;
        $validated['pic_name'] = $request->pic_name;
        $validated['pic_phone'] = $request->pic_phone;
        $validated['pic_email'] = $request->pic_email;

        Vendor::where('id', $id)->update($validated);
        return redirect('/vendor/' . $id)->with('success', 'Vendor has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vendor::destroy($id);
        return back();
    }
}
