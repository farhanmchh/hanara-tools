<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mom;
use App\Models\MomTopic;
use App\Models\MomAttendee;
use App\Models\MomIssue;
use PDF;
use Mail;

class MomController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.mom.index', [
      'title' => 'Minutes of Meeting',
      'moms' => Mom::all()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('dashboard.mom.create', [
      'title' => 'Create Minutes of Meeting'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validated = $request->validate([
      'subject' => ['required'],
      'date' => ['required'],
      'location' => ['required'],
      'note_taker' => ['required'],
      'from' => ['required'],
      'to' => ['required'],
      'duration' => ['required'],
      'tnc' => ['required'],
      'verified_by' => ['required'],
      'verified_date' => ['required']
    ]);


    $mom = Mom::create($validated);

    foreach ($request->topic as $key => $topic) {
      $momTopic = [
        'mom_id' => $mom->id,
        'topic' => $request->topic[$key]
      ];
      MomTopic::create($momTopic);
    }

    foreach ($request->name as $key => $name) {
      $momAttendee = [
        'mom_id' => $mom->id,
        'name' => $request->name[$key],
        'company' => $request->company[$key],
        'position' => $request->position[$key],
        'email' => $request->email[$key]
      ];
      MomAttendee::create($momAttendee);
    }

    foreach ($request->description as $key => $description) {
      $momIssue = [
        'mom_id' => $mom->id,
        'description' => $request->description[$key],
        'type' => $request->type[$key],
        'assigned' => $request->assigned[$key],
        'due_date' => $request->due_date[$key],
        'status' => $request->status[$key],
        'date_resolved' => $request->date_resolved[$key]
      ];
      MomIssue::create($momIssue);
      // return response()->json($request->type);
    }

    return redirect('/mom')->with('success', 'Minutes of Meeting has been created!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.mom.show', [
      'title' => 'Detail MoM',
      'mom' => Mom::where('id', $id)->first()
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    return view('dashboard.mom.edit', [
      'title' => 'Edit MoM',
      'mom' => Mom::where('id', $id)->first()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'subject' => ['required'],
      'date' => ['required'],
      'location' => ['required'],
      'note_taker' => ['required'],
      'from' => ['required'],
      'to' => ['required'],
      'duration' => ['required'],
      'tnc' => ['required'],
      'verified_by' => ['required'],
      'verified_date' => ['required']
    ]);

    $mom = Mom::find($id);

    MomTopic::where('mom_id', $mom->id)->delete();

    foreach ($request->topic as $key => $topic) {
      $momTopic = [
        'mom_id' => $mom->id,
        'topic' => $request->topic[$key]
      ];
      MomTopic::create($momTopic);
    }

    MomAttendee::where('mom_id', $mom->id)->delete();

    foreach ($request->name as $key => $name) {
      $momAttendee = [
        'mom_id' => $mom->id,
        'name' => $request->name[$key],
        'company' => $request->company[$key],
        'position' => $request->position[$key],
        'email' => $request->email[$key]
      ];
      MomAttendee::create($momAttendee);
    }

    MomIssue::where('mom_id', $mom->id)->delete();

    foreach ($request->description as $key => $description) {
      $momIssue = [
        'mom_id' => $mom->id,
        'description' => $request->description[$key],
        'type' => $request->type[$key],
        'assigned' => $request->assigned[$key],
        'due_date' => $request->due_date[$key],
        'status' => $request->status[$key],
        'date_resolved' => $request->date_resolved[$key]
      ];
      MomIssue::create($momIssue);
    }

    $mom->update($validated);
    return redirect('/mom')->with('success', 'Minutes of Meeting has been updat ed!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  public function printPdf($id)
  {
    $data = [
      'mom' => Mom::where('id', $id)->first()
    ];

    return PDF::loadview('print.printMom', $data)->setPaper('a4', 'potrait')->stream();
  }

  public function email($id)
  {
    $mom = Mom::find($id);

    $data = ['mom' => $mom->first()];
    
    $pdf = PDF::loadview('print.printMom', $data);

    $attendees = MomAttendee::where('mom_id', $id)->get();
    
    foreach ($attendees as $attendee) {
      Mail::send(
        'dashboard.mom.momMail',
        ['name' => $attendee->name],
        function ($mail) use ($attendee, $mom, $pdf) {
          $mail->to($attendee->email)->subject($mom->subject)->attachData($pdf->output(), "MoM - {$mom->subject}.pdf");
          $mail->from('dudu@hanara.id', 'PT Hanara Sentra Teknologi');
        }
      );
    }

    return back()->with('success', 'Mom has been sended!');
  }
}
