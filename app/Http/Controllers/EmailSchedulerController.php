<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EmailScheduler;
use App\Models\Customer;
use App\Models\Company;
use App\Models\Category;
use App\Models\CustomerType;
use App\Jobs\SendMailJob;
use App\Mail\NewArrivals;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class EmailSchedulerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.email_scheduler.index', [
      'title' => 'Email scheduler',
      'email_scheduler' => EmailScheduler::orderBy('created_at', 'desc')->get()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('dashboard.email_scheduler.create', [
      'title' => 'Create Email Scheduler',
      'company' => Company::first(),
      'categories' => Category::all(),
      'customer_type' => CustomerType::all()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
  
    //JIKA ADA DATA YANG DIKIRIMKAN
    if ($request->hasFile('upload')) {
      $file = $request->file('upload'); //SIMPAN SEMENTARA FILENYA KE VARIABLE
      $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME); //KITA GET ORIGINAL NAME-NYA
      //KEMUDIAN GENERATE NAMA YANG BARU KOMBINASI NAMA FILE + TIME
      $fileName = $fileName . '_' . time() . '.' . $file->getClientOriginalExtension();

      $file->move(public_path('uploads'), $fileName); //SIMPAN KE DALAM FOLDER PUBLIC/UPLOADS

      //KEMUDIAN KITA BUAT RESPONSE KE CKEDITOR
      $ckeditor = $request->input('CKEditorFuncNum');
      $url = asset('uploads/' . $fileName); 
      $msg = 'Image uploaded successfully'; 
      //DENGNA MENGIRIMKAN INFORMASI URL FILE DAN MESSAGE
      $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

      //SET HEADERNYA
      @header('Content-type: text/html; charset=utf-8'); 
      return $response;
    }

    $messages = [
      'required' => ':attribute required!',
    ];

    $this->validate($request,[
        'subject' => 'required',
        'description' => 'required',
        'category' => 'required',
        'description' => 'required',
        'start' => 'required',
        'end' => 'required',
        'frequency' => 'required',
    ],$messages);

    $scheduler = new EmailScheduler();

    if($request->category ==   'Database Only') {
      $getName = Customertype::where('name', $request->category)->first();
    }
    else if($request->category ==   'Customer') {
      $getName = Customertype::where('name', $request->category)->first();
    }
    else if($request->category ==  'One Time Customer') {
      $getName = Customertype::where('name', $request->category)->first();
    }
     else {
      $getId = $request->category;
    }
    
    $scheduler->description = $request->description;
    $scheduler->subject = $request->subject;
    $scheduler->from_ = $request->from;
    $scheduler->frequency = $request->frequency;
    $scheduler->delivered = 0;

    if($request->date) {
      $scheduler->day = $request->date;
    } else {
      $scheduler->day = $request->day;
    }

    $scheduler->time = $request->time;

    if ($request->category == 'One Time Customer') {
        $scheduler->customer_type_id = $getName->id;
    }
    elseif ($request->category == 'Database Only') {
      $scheduler->customer_type_id = $getName->id;
    }
    elseif ($request->category == 'Customer') {
      $scheduler->customer_type_id = $getName->id;
    }
    else{
        $scheduler->category_id =  $getId;
    }

    $scheduler->start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
    $scheduler->end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
    $scheduler->save();
  
    return redirect('/email_scheduler')->with('success', 'Email has been created!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.email_scheduler.show', [
      'title' => 'Detail Email Scheduler',
      'email_blast' => EmailScheduler::where('id', $id)->first()
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    return view(
      'dashboard.email_scheduler.edit',
      [
        'title' => 'Edit Email Scheduler',
        'company' => Company::first(),
        'edit' => EmailScheduler::where('id', $id)->first(),
        'categories' => Category::all(),
        'customer_type' => CustomerType::all()
      ]
    );
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'subject' => ['required'],
      'description' => ['required'],
    ]);

    if ($request->category == 'Chouse Category') {
      return redirect()->back()->with('error', 'Gagal dikirim, silahkan pilih salah satu category');
    }

    $validated['subject'] = $request->subject;
    $validated['description'] = $request->description;
    if ($request->category == 'All') {
      $validated['category_id'] = null;
    } else {
      $validated['category_id'] = $request->category;
    }
    //   return response()->json($validated['category_id']);
    $validated['from_'] = $request->from;

    $email_blast = Blasting::where('id', $id)->update($validated);;

    return redirect('/email')->with('success', 'Schedule has been updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    EmailScheduler::destroy($id);
    return back()->with('success', 'Row has been deleted!');
  }

}
