<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
  public function index()
  {
    return view('auth/login');
  }

  public function authenticate(Request $request)
  {
    $credentials = $request->validate(
      [
        'email' => ['required', 'email'],
        'password' => ['required']
      ],
      [
        'email.required' => 'Email field is require',
        'email.email' => 'Email field must be a valid email',
        'password.required' => 'Password is require'
      ]
    );

    if (Auth::attempt($credentials)) {
      $request->session()->regenerate();

      return redirect('/dashboard');
    }

    return back()->with('error', 'Login error!');
  }

  public function pdf()
  {
    return view('print_pdf.print_pdf');
  }


  public function logout(Request $request)
  {
    Auth::logout();

    $request->session()->invalidate();
    $request->session()->regenerateToken();

    return redirect('/')->with('success', 'Thanks!');
  }
}
