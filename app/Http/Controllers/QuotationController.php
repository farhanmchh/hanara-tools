<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Quotation;
use App\Models\Item;
use App\Models\Customer;
use App\Models\Tnc;
use App\Models\QuotationItem;
use App\Mail\QuotationMail;
use Mail;
use PDF;

class QuotationController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.quotation.index', [
      'title' => 'Quotation',
      'quotations' => Quotation::all()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $item = Item::count();
    $customer = Customer::count();

    if ($item > 0 && $customer > 0) {
      $check_code = Quotation::withTrashed()->whereYear('created_at', Carbon::now()->year);

      $id_code = 101;

      if ($check_code->count() > 0) {
        $id_code += $check_code->count();
      }

      $roman_number = romanNumber(Carbon::now()->month);
      $year = Carbon::now()->year;

      $code = "{$id_code}/Quotation/Hanara/{$roman_number}/{$year}";

      return view('dashboard.quotation.create', [
        'title' => 'Quotation',
        'items' => Item::all(),
        'customers' => Customer::all(),
        'company' => Company::first(),
        'tncs' => Tnc::all(),
        'code' => $code
      ]);
    } else {
      return back()->with('error', 'Item or Customer is empty, please add it first.');
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validated = $request->validate([
      'customer_id' => ['required'],
      'tnc_id' => ['required'],
      'subject' => ['required'],
      'date' => ['required']
    ]);

    $validated['code'] = $request->code;
    $validated['total'] = $request->total;
    $validated['discount'] = $request->discount;
    $validated['grand_total'] = $request->grand_total;

    $quotation = Quotation::create($validated);

    foreach ($request->item as $key => $item) {
      $itemDetail = Item::find($request->item[$key]);
      $quotationItem = [
        'quotation_id' => $quotation->id,
        'item_id' => $itemDetail->id,
        'name' => $itemDetail->name,
        'price' => $itemDetail->price,
        'description' => $itemDetail->description,
        'quantity' => $request->quantity[$key],
        'frequency' => $request->frequency[$key],
        'amount' => $request->amount[$key]
      ];
      QuotationItem::create($quotationItem);
    }
    return redirect('/quotation')->with('success', 'Quotation has been created!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.quotation.show', [
      'title' => 'Detail Quotation',
      'company' => Company::first(),
      'quotation' => Quotation::where('id', $id)->first()
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    // $quotation = Quotation::where('id', $id)->first();
    // return response()->json($quotation->quotationItem);
    return view('dashboard.quotation.edit', [
      'title' => 'Edit Quotation',
      'customers' => Customer::all(),
      'company' => Company::first(),
      'items' => Item::all(),
      'tncs' => Tnc::all(),
      'quotation' => Quotation::where('id', $id)->first()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $quotationItem = $request->validate([
      'item' => ['required']
    ]);
    
    // return response()->json('ok');

    $validated = $request->validate([
      'customer_id' => ['required'],
      'tnc_id' => ['required'],
      'subject' => ['required'],
      'date' => ['required'],
      'status' => ['required']
    ]);

    $validated['code'] = $request->code;
    $validated['total'] = $request->total;
    $validated['discount'] = $request->discount;
    $validated['grand_total'] = $request->grand_total;

    $quotation = Quotation::find($id);

    QuotationItem::where('quotation_id', $quotation->id)->delete();
    
    
    foreach ($request->item as $key => $item) {
      $itemDetail = Item::find($request->item[$key]);
      // return response()->json($itemDetail);

      $quotationItem = [
        'quotation_id' => $quotation->id,
        'item_id' => $itemDetail->id,
        'name' => $itemDetail->name,
        'price' => $itemDetail->price,
        'description' => $itemDetail->description,
        'quantity' => $request->quantity[$key],
        'frequency' => $request->frequency[$key],
        'amount' => $request->amount[$key]
      ];

      QuotationItem::create($quotationItem);
    }

    $quotation->update($validated);

    return redirect('/quotation')->with('success', 'Quotation has been updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Quotation::destroy($id);
    return back()->with('success', 'Quotation has been deleted!');
  }

  // ...

  public function getCustomer($id)
  {
    $customer = Customer::where('id', $id)->get('pic_name');

    return response()->json($customer);
  }

  public function getPrice($id)
  {
    $price = Item::where('id', $id)->get('price');
    return response()->json($price);
  }

  public function printPdf($id)
  {
    $data = [
      'company' => Company::first(),
      'quotation' => Quotation::where('id', $id)->first()
    ];

    return PDF::loadview('print.printQuotation', $data)->setPaper('a4', 'potrait')->stream();
  }

  public function email($id)
  {
    $quotation = Quotation::find($id);

    $data = [
      'company' => Company::first(),
      'quotation' => $quotation->first()
    ];

    $pdf = PDF::loadview('print.printQuotation', $data);

    Mail::send(
      'dashboard.quotation.quotationMail',
      $data,
      function ($mail) use ($quotation, $pdf) {
        $mail->to($quotation->customer->email)->subject($quotation->subject)->attachData($pdf->output(), 'Quotation.pdf');
        $mail->from('dudu@hanara.id', 'PT Hanara Sentra Teknologi');
      }
    );

    $quotation->status = 'waiting response';
    $quotation->save();
    return redirect('/quotation')->with('success', 'Send email successfully!');
  }
}
