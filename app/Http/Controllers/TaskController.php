<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Models\Task;
use App\Models\Project;
use App\Models\Employee;
use App\Models\ProjectCategory;



class TaskController extends Controller
{
 
  public function index()
  {
    return view('dashboard.task.index', [
      'title' => 'Task',
      'project' => Project::all(),
      'employee' => Employee::all(),
      'project_category' => ProjectCategory::all(),
      'tasks' => Task::all()

    ]);
  }


  public function create()
  {
    
      return view('dashboard.task.create', [
        'title' => 'Task',
        'employee' => Employee::all(),
        'project' => Project::all(),
        'project_category' => ProjectCategory::all(),
        'tasks' => Task::all()
      ]);

      return back()->with('error', 'Name or Description is empty, please add it first.');
    }

  public function store(Request $request)
  {
    $validated = $request->validate([
        'name' => ['required'],
        'description' => ['required'],
        'deadline' => ['required'],
        'employee_id' => ['required'],
        'projects_id' => ['required'],
        'project_category_id' => ['required'],
        'task_status' => ['required']

    ]);

    Task::create($validated);

    return redirect('/task')->with('success', 'Task has been created!');
  }


  public function show($id)
  {
    return view('dashboard.task.show', [
        'title' => 'Detail Task',
        'employee' => Employee::all(),
        'project' => Project::all(),
        'project_category' => ProjectCategory::all(),
        'task' => Task::where('id', $id)->first()
    ]);
  }

  public function edit($id)
  {
    return view('dashboard.task.edit', [
        'title' => 'Edit Task',
        'employee' => Employee::all(),
        'project' => Project::all(),
        'project_category' => ProjectCategory::all(),
        'task' => Task::where('id', $id)->first()
    ]);
  }

  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'description' => ['required'],
      'deadline' => ['required'],
      'employee_id' => ['required'],
      'projects_id' => ['required'],
      'project_category_id' => ['required'],
      'task_status' => ['required']
    ]);

    Task::where('id', $id)->update($validated);

    return redirect('/task')->with('success', 'Task has been updated!');
  }


  public function destroy($id)
  {
    Task::destroy($id);
    return back()->with('success', 'Task has been deleted!');
  }

}
