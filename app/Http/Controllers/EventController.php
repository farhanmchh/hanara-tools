<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Event;

class EventController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $data_event = Event::query();
      // echo '<pre>';
    if (request()->start_date || request()->end_date) {    
      $date['date_start'] = request()->start_date;
      $date['date_end'] = request()->end_date;

      $date_start = date('Y-m-d 00:00:00',strtotime($date['date_start']));
      $date_end = date('Y-m-d 23:59:59',strtotime($date['date_end']));

      $data_event->whereBetween('date', [$date_start, $date_end]);
    }
    // print_r($event);
    // exit;

   
    if (request()->search){
      $keyword = $request->search;
      $data_event->where('name', 'like', "%" . $keyword . "%")->orWhere('date', 'like', '%' . $keyword . '%');
    }
    $event = $data_event->get();

    // echo '<pre>';print_r($event);exit;
      return view('dashboard.event.index', [
        'title' => 'Events',
        'event' => $event,
        'keyword'
      ]);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function create()
  {
    return view('dashboard.event.create', [
      'title' => 'Add Event',
      'event' => Event::all()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validated = $request->validate([
        'name' => ['required'],
        'description' => ['required'],
        'date' => ['required'],
        'status' => ['required']
      ]);
    Event::create($validated);
    return redirect('/event')->with('success', 'Event has been added!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.event.show', [
      'title' => 'Detail Event',
      'event' => Event::where('id', $id)->first()
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id)
  {
    return view('dashboard.event.edit', [
      'title' => 'Edit Event',
      'event' => Event::where('id', $id)->first()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'description' => ['required'],
      'date' => ['required'],
      'status' => ['required']

    ]);

    Event::where('id', $id)->update($validated);

    return redirect('/event')->with('success', 'Event has been updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy($id)
  {
    Event::destroy($id);
    return back()->with('success', 'Event has been deleted!');
  }
}
