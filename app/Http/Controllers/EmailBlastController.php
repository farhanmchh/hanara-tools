<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EmailBlast;
use App\Models\Customer;
use App\Models\Company;
use App\Models\Category;
use App\Models\CustomerType;
use Mail;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class EmailBlastController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.blasting.index', [
      'title' => 'Email blast',
      'email_blast' => EmailBlast::orderBy('created_at', 'desc')->get()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('dashboard.blasting.create', [
      'title' => 'Create Email Blast',
      'company' => Company::first(),
      'categories' => Category::all(),
      'customer_type' => CustomerType::all()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    if ($request->hasFile('upload')) {
      $file = $request->file('upload'); 
      $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME); 
      
      $fileName = $fileName . '_' . time() . '.' . $file->getClientOriginalExtension();

      $file->move(public_path('uploads'), $fileName);

      
      $ckeditor = $request->input('CKEditorFuncNum');
      $url = asset('uploads/' . $fileName); 
      $msg = 'Image uploaded successfully'; 
      
      $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

      @header('Content-type: text/html; charset=utf-8'); 
      return $response;
    }

    $messages = [
      'required' => ':attribute required!',
    ];

    $this->validate($request,[
        'subject' => 'required',
        'description' => 'required',
        'blast_at' => 'required',
        'category' => 'required',
    ],$messages);

    if($request->category == 'Database Only') {
      $getName = Customertype::where('name', $request->category)->first();
    }
    else if($request->category == 'Customer') {
      $getName = Customertype::where('name', $request->category)->first();
    }
    else if($request->category == 'One Time Customer') {
      $getName = Customertype::where('name', $request->category)->first();
    }
     else {
      $getId = $request->category;
    }

    $validated = $request->validate([
      'subject' => ['required'],
      'description' => ['required'],
      'from' => ['required']
    ]);

    if ($request->category == 'Database Only') {
      $validated['customer_type_id'] = $getName->id;
    }
    else if ($request->category == 'Customer') {
      $validated['customer_type_id'] = $getName->id;
    }
    else if ($request->category == 'One Time Customer') {
      $validated['customer_type_id'] = $getName->id;
    }
    else{
      $validated['category_id'] = $getId;
    }
    $validated['from_'] = $request->from;
    
    $validated['blast_at'] = Carbon::parse($request->blast_at)->format('Y-m-d H:i:s');

    EmailBlast::create($validated);

    return redirect('/email_blast')->with('success', 'Email has been created!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.blasting.show', [
      'title' => 'Detail Template',
      'email_blast' => EmailBlast::where('id', $id)->first(),
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    return view(
      'dashboard.blasting.edit',
      [
        'title' => 'Detail Email',
        'company' => Company::first(),
        'edit' => EmailBlast::where('id', $id)->first(),
        'categories' => Category::all(),
        'customer_type' => CustomerType::all(),
        'data_user' => Category::where('id', $id)->first(),
      ]
    );
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    
    $find = EmailBlast::find($id);

    EmailBlast::where('category_id', $request->id)->delete();

    return response()->json($delete);


    //JIKA ADA DATA YANG DIKIRIMKAN
    if ($request->hasFile('upload')) {
      $file = $request->file('upload'); //SIMPAN SEMENTARA FILENYA KE VARIABLE
      $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME); //KITA GET ORIGINAL NAME-NYA
      //KEMUDIAN GENERATE NAMA YANG BARU KOMBINASI NAMA FILE + TIME
      $fileName = $fileName . '_' . time() . '.' . $file->getClientOriginalExtension();

      $file->move(public_path('uploads'), $fileName); //SIMPAN KE DALAM FOLDER PUBLIC/UPLOADS

      //KEMUDIAN KITA BUAT RESPONSE KE CKEDITOR
      $ckeditor = $request->input('CKEditorFuncNum');
      $url = asset('uploads/' . $fileName); 
      $msg = 'Image uploaded successfully'; 
      //DENGNA MENGIRIMKAN INFORMASI URL FILE DAN MESSAGE
      $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";

      //SET HEADERNYA
      @header('Content-type: text/html; charset=utf-8'); 
      return $response;
    }

  
    $messages = [
      'required' => ':attribute wajib diisi !!',
    ];

    $this->validate($request,[
        'subject' => 'required',
        'description' => 'required',
        'blast_at' => 'required',
        'category' => 'required',
    ],$messages);

    if($request->category == 'Database Only') {
      $getName = Customertype::where('name', $request->category)->first();
    }
    else if($request->category == 'Customer') {
      $getName = Customertype::where('name', $request->category)->first();
    }
    else if($request->category == 'One Time Customer') {
      $getName = Customertype::where('name', $request->category)->first();
    }
     else {
      $getId = $request->category;
    }

    if ($request->category == 'Database Only') {
      $validated['customer_type_id'] = $getName->id;
    }
    else if ($request->category == 'Customer') {
      $validated['customer_type_id'] = $getName->id;
    }
    else if ($request->category == 'One Time Customer') {
      $validated['customer_type_id'] = $getName->id;
    }
    else{
      $validated['category_id'] = $getId;
    }
    $validated['from_'] = $request->from;
    $validated['description'] = $request->description;
    $validated['subject'] = $request->subject;


    $validated['blast_at'] = Carbon::parse($request->blast_at)->format('Y-m-d H:i:s');
    
    $email_blast = EmailBlast::where('id', $id)->update($validated);;

    return redirect('/email_blast')->with('success', 'Email has been updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    EmailBlast::destroy($id);
    return back()->with('success', 'Row has been deleted!');
  }

  public function unsubscribe($id)
  {
    $customer = Customer::where('id', $id)->first();;
    $customer->unsubscribe = Carbon::now()->format('Y-m-d H:i:s');
    $customer->save();
    
    return view('dashboard.customers.unsubscribe');
  }


}
