<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Category;
use App\Models\CustomerType;

class CustomerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.customers.index', [
      'title' => 'Customers',
      'customers' => Customer:: paginate(10)
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function create()
  {
    $category = Category::count();

    if ($category > 0) {
      return view('dashboard.customers.create', [
        'title' => 'Add Customer',
        'categories' => Category::all()
      ]);
    } else {
      return back()->with('error', 'Category is empty, please add category first.');
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'phone' => ['required', 'numeric', 'unique:customers'],
      'email' => ['required', 'email', 'unique:customers'],
      'address' => ['required'],
      'province' => ['required'],
      'city' => ['required'],
      'category_id' => ['required'],
      'type' => ['required'],
      'website' => [],
      'pic_email' => []
    ]);
    Customer::create($validated);
    return redirect('/customer')->with('success', 'Customer has been added!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.customers.show', [
      'title' => 'Detail Customer',
      'customer' => Customer::where('id', $id)->first(),
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id)
  {
    return view('dashboard.customers.edit', [
      'title' => 'Edit Customer',
      'categories' => Category::all(),
      'types' => CustomerType::all(),
      'customer' => Customer::where('id', $id)->first()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'phone' => ['required', 'numeric'],
      'email' => ['required', 'email'],
      'address' => ['required'],
      'province' => ['required'],
      'city' => ['required'],
      'category_id' => ['required'],
      'customer_type_id' => ['required'],
      'website' => [],
      'pic_name' => [],
      'pic_phone' => [],
      'pic_email' => []
    ]);

    Customer::where('id', $id)->update($validated);

    return redirect('/customer')->with('success', 'Customer has been updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy($id)
  {
    Customer::destroy($id);
    return back()->with('success', 'Customer has been deleted!');
  }
}
