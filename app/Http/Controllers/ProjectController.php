<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Models\Project;
use App\Models\Customer;


class ProjectController extends Controller
{
 
  public function index()
  {
    return view('dashboard.project.index', [
      'title' => 'Project',
      'customers' => Customer::all(),
      'projects' => Project::all()
    ]);
  }


  public function create()
  {
    
      return view('dashboard.project.create', [
        'title' => 'Project',
        'customers' => Customer::all(),
        'projects' => Project::all()
      ]);

      return back()->with('error', 'Name or Client is empty, please add it first.');
    }

  public function store(Request $request)
  {
    $validated = $request->validate([
        'name' => ['required'],
        'description' => ['required'],
        'deadline' => ['required'],
        'customer_id' => ['required']
    ]);

    Project::create($validated);

    return redirect('/project')->with('success', 'Project has been created!');
  }


  public function show($id)
  {
    return view('dashboard.project.show', [
      'title' => 'Detail Project',
      'customers' => Customer::all(),
      'project' => Project::where('id', $id)->first()
    ]);
  }

  public function edit($id)
  {
    return view('dashboard.project.edit', [
      'title' => 'Edit project',
      'customers' => Customer::all(),
      'project' => Project::where('id', $id)->first()
    ]);
  }

  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'description' => ['required'],
      'deadline' => ['required'],
      'customer_id' => ['required']
    ]);

    Project::where('id', $id)->update($validated);

    return redirect('/project')->with('success', 'Project has been updated!');
  }


  public function destroy($id)
  {
    Project::destroy($id);
    return back()->with('success', 'Project has been deleted!');
  }

  public function getCustomer($id)
  {
    $customer = Customer::where('id', $id)->get('pic_name');

    return response()->json($customer);
  }

}
