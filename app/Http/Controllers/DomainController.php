<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Domain;
use App\Models\NameServer;
use App\Models\Vendor;

class DomainController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('dashboard.domain.index', [
      'title' => 'Domain',   
      'domains' => Domain::all()
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $customer = Customer::count();
    $vendor = Vendor::count();

    if ($customer == 0) {
      return back()->with('error', "Cannot add domain, you don't have CUSTOMER data");
    }
    if ($vendor == 0) {
      return back()->with('error', "Cannot add domain, you don't have VENDOR data");
    }

    return view('dashboard.domain.create', [
      'title' => 'Create Domain',
      'customers' => Customer::all(),
      'vendors' => Vendor::all()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'customer_id' => ['required'],
      'vendor_id' => ['required'],
      'expired' => ['required'],
    ]);

    if ($request->cpanel) {
      $validated += $request->validate([
        'cpanel' => ['required'],
        'user_cpanel' => ['required'],
        'password_cpanel' => ['required'],
      ]);
    } else {
      $validated['cpanel'] = $request->cpanel;
      $validated['user_cpanel'] = $request->user_cpanel;
      $validated['password_cpanel'] = $request->password_cpanel;
    }

    $validated['hosting'] = $request->hosting;

    $domain = Domain::create($validated);

    if ($request->name_server) { 
      foreach ($request->name_server as $key => $name_server) {
        $nameServer = [
          'domain_id' => $domain->id,
          'name' => $request->name_server[$key]
        ];
        NameServer::create($nameServer);
      }
    }
    
    return redirect('/domain')->with('success', 'Create domain successfully!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.domain.show', [
      'title' => 'Detail Domain',
      'domain' => Domain::where('id', $id)->first()
    ]);
  }
  
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    return view('dashboard.domain.edit', [
      'title' => 'Edit Domain',
      'customers' => Customer::all(),
      'vendors' => Vendor::all(),
      'domain' => Domain::where('id', $id)->first()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'customer_id' => ['required'],
      'vendor_id' => ['required'],
      'expired' => ['required']
    ]);

    if ($request->cpanel) {
      $validated += $request->validate([
        'cpanel' => ['required'],
        'user_cpanel' => ['required'],
        'password_cpanel' => ['required'],
      ]);
    } else {
      $validated['cpanel'] = $request->cpanel;
      $validated['user_cpanel'] = $request->user_cpanel;
      $validated['password_cpanel'] = $request->password_cpanel;
    }

    $validated['hosting'] = $request->hosting;

    $domain = Domain::find($id);

    NameServer::where('domain_id', $domain->id)->delete();

    if ($request->name_server) {   
      foreach ($request->name_server as $key => $name_server) {
        $nameServer = [
          'domain_id' => $domain->id,
          'name' => $request->name_server[$key]
        ];
        NameServer::create($nameServer);
      }
    }

    $domain->update($validated);
    return redirect("/domain")->with('success', 'Domain has been updated');
  }

  public function destroy($id)
  {
    Domain::destroy($id);
    return redirect('/domain')->with('success', 'Domain has been deleted!');
  }
}
