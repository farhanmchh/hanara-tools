<?php

namespace App\Http\Controllers;

use App\Models\Tnc;
use App\Models\Quotation;
use Illuminate\Http\Request;

class TncController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.tnc.index', [
            'title' => 'Terms & Conditions',
            'tncs' => Tnc::all()
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.tnc.create', [
            'title' => 'Create Terms & Conditions',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'name' => ['required'],
            'body' => ['required']
        ]);

        Tnc::create($validated);
        return redirect('/tnc')->with('success', 'TNC has been created!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tnc  $tnc
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('dashboard.tnc.show', [
            'title' => 'Detail TNC',
            'tnc' => Tnc::where('id', $id)->first()
        ]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tnc  $tnc
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.tnc.edit', [
            'title' => 'Detail TNC',
            'tnc' => Tnc::where('id', $id)->first()
        ]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tnc  $tnc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => ['required'],
            'body' => ['required']
        ]);
    
        Tnc::where('id', $id)->update($validated);
        return redirect('/tnc')->with('success', 'TNC has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tnc  $tnc
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check_quotation = Quotation::where('tnc_id', $id)->first();

        if ($check_quotation) {
            return back()->with('error', 'Cannot delete, this TNC have a Quotation!');
        }

        Tnc::where('id', $id)->delete();
        return redirect('/tnc')->with('success', 'TNC has been deleted!');
    }
}
