<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Models\Project;
use App\Models\ProjectCategory;



class ProjectCategoryController extends Controller
{
 
  public function index()
  {
    return view('dashboard.project_category.index', [
      'title' => 'Project Category',
      'project' => Project::all(),
      'project_category' => ProjectCategory::all()
    ]);
  }


  public function create()
  {
    
      return view('dashboard.project_category.create', [
        'title' => 'Create Project Category',
        'project' => Project::all(),
        'project_category' => ProjectCategory::all()
      ]);

      return back()->with('error', 'Name or Project is empty, please add it first.');
  }

  public function store(Request $request)
  {
    $validated = $request->validate([
        'name' => ['required'],
        'project_id' => ['required']
    ]);

    ProjectCategory::create($validated);

    return redirect('/project_category')->with('success', 'Category Project has been created!');
  }


  public function show($id)
  {
    return view('dashboard.project_category.show', [
      'title' => 'Detail Project Category',
      'project' => Project::all(),
      'project_category' => ProjectCategory::where('id', $id)->first()
    ]);
  }

  public function edit($id)
  {
    return view('dashboard.project_category.edit', [
      'title' => 'Edit Project Category',
      'project' => Project::all(),
      'project_category' => ProjectCategory::where('id', $id)->first()
    ]);
  }

  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'name' => ['required'],
      'project_id' => ['required']
    ]);

    ProjectCategory::where('id', $id)->update($validated);

    return redirect('/project_category')->with('success', 'Category Project has been updated!');
  }


  public function destroy($id)
  {
    ProjectCategory::destroy($id);
    return back()->with('success', 'Category Project has been deleted!');
  }

  public function getProjectCategory(Request $request)
  {
    $validated = $request->validate([
        'id' => ['required','numeric'],
    ]);

    $id = $request->id;

    $category = ProjectCategory::where('project_id', $id)->get(['id','name']);

    if(count($category) == 0){
      return response()->json([
          'status' => 'error',
          'messages' => "Category Doesn't Exist! Please Create Category First"
      ]);
    }

    return response()->json([
        'status' => 'success',
        'messages' => 'Data Found',
        'data' => $category,
    ]);
  }

//   public function getCustomer($id)
//   {
//     $customer = Customer::where('id', $id)->get('pic_name');

//     return response()->json($customer);
//   }

}
