<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Models\Employee;

class EmployeeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

      return view('dashboard.employee.index', [
        'title' => 'Employees',
        'employee' => Employee::all()
      ]);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function create()
  {
    return view('dashboard.employee.create', [
      'title' => 'Add Employee',
      'employee' => Employee::all()
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validated = $request->validate([
        'name' => ['required'],
      ]);
    Employee::create($validated);
    return redirect('/employee')->with('success', 'Employee has been added!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return view('dashboard.employee.show', [
      'title' => 'Detail Employee',
      'employee' => Employee::where('id', $id)->first()
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function edit($id)
  {
    return view('dashboard.employee.edit', [
      'title' => 'Edit Employee Data',
      'employee' => Employee::where('id', $id)->first()
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function update(Request $request, $id)
  {
    $validated = $request->validate([
      'name' => ['required'],
    ]);

    Employee::where('id', $id)->update($validated);

    return redirect('/employee/' . $id)->with('success', 'Employee name has been updated!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function destroy($id)
  {
    Employee::destroy($id);
    return back()->with('success', 'Employee has been deleted!');
  }
}
