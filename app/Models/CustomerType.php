<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Customer;
use App\Models\EmailBlast;

class CustomerType extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'customer_type';

    public function blasting()
    {
        return $this->hasMany(EmailBlast::class);
    }

    public function email_scheduler()
    {
        return $this->hasMany(EmailScheduler::class);
    }
    
    public function customer()
    {
        return $this->hasMany(Customer::class);
    }
}
