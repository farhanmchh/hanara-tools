<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Employee;
use App\Models\Project;
use App\Models\ProjectCategory;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $table = 'tasks';

    public function project()
    {
        return $this->belongsTo(Project::class, 'projects_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function project_category()
    {
        return $this->belongsTo(ProjectCategory::class);
    }

}
