<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailBlast extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $table = 'email_blast';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function customerType()
    {
        return $this->belongsTo(CustomerType::class);
    }
}
