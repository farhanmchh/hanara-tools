<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MomTopic;
use App\Models\MomAttendee;
use App\Models\MomIssue;

class Mom extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $table = 'moms';

    public function momTopic()
    {
        return $this->hasMany(MomTopic::class);
    }

    public function momAttendee()
    {
        return $this->hasMany(MomAttendee::class);
    }

    public function momIssue()
    {
        return $this->hasMany(MomIssue::class);
    }
}
