<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Quotation;

class Tnc extends Model
{
    use HasFactory;

    protected $guarded = ['id']; 
    protected $table = 'tncs';
    
    public function quotation()
    {
        return $this->hasMany(Quotation::class);
    }
}
