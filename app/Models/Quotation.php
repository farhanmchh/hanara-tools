<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Customer;
use App\Models\QuotationItem;
use App\Models\Tnc;

class Quotation extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $table = 'quotations';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function quotationItem()
    {
        return $this->hasMany(QuotationItem::class);
    }

    public function tnc()
    {
        return $this->belongsTo(Tnc::class);
    }
}
