<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Mom;

class MomTopic extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'mom_topics';

    public function mom()
    {
        return $this->belongsTo(Mom::class);
    }
}
