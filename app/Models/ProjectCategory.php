<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Task;
use App\Models\Project;

class ProjectCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $table = 'project_categories';

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function task()
    {
        return $this->hasMany(Task::class);
    }
}