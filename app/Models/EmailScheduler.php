<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailScheduler extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'email_schedulers';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function customerType()
    {
        return $this->belongsTo(CustomerType::class);
    }
}
