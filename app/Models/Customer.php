<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Quotation;
use App\Models\Project;
use App\Models\Category;
use App\Models\Domain;
use App\Models\CustomerType;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];
    protected $table = 'customers';

    public function quotation()
    {
        return $this->hasMany(Quotation::class);
    }

    public function project()
    {
        return $this->hasMany(Project::class);
    }

    public function domain()
    {
        return $this->hasMany(Domain::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function customerType()
    {
        return $this->belongsTo(CustomerType::class);
    }
}
