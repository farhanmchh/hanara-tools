<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use App\Models\EmailBlast;
use App\Models\Customer;
use App\Models\CustomerType;
use Carbon\Carbon;
use Mail;
use App\Mail\NewArrivals;

class EmailBlasts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:blast';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Schedule $schedule)
    {

          $email_blast    =   EmailBlast::where("blast_at", Carbon::now()->format('Y-m-d H:i'))->each(function($i){
            $i->delivered = 1;
            $i->save();   
          });

          $email_blasts    =   EmailBlast::where("blast_at", Carbon::now()->format('Y-m-d H:i'))->get();
            foreach ($email_blasts as $item) {

                if($item->category_id){
                  $customers      = Customer::where("category_id", $item->category_id)->where('unsubscribe', NULL)->get();
                                            
                } else {
                  $customers      = Customer::where("customer_type_id", $item->customer_type_id)->where('unsubscribe', NULL)->get();          
                }            
                
                foreach ($customers as $customer) {
                  $data = array(
                    'id' => $customer->id,
                    'getEmail' => $customer->email,
                    'name' => $customer->name,
                    'phone' => $customer->phone,
                    'subject' => $item->subject,
                    'description' => $item->description
                  );

                  Mail::send('dashboard.blasting.template', $data, function ($mail) use ($customer, $item){
                    $mail->to($customer->email, 'no-reply')
                      ->subject($item->subject);
                    $mail->from('dudu@hanara.id', 'Hanara Sentra Teknologi');
                  });
                }//end foreach $customers


            } //end foreach $email_blast

  

        // echo "operation done";
      }
}
