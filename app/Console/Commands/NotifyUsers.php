<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\EmailScheduler;
use App\Models\Customer;
use App\Models\CustomerType;
use Carbon\Carbon;
use App\Jobs\SendMailJob;
use App\User;
use Mail;
use App\Mail\NewArrivals;
use Illuminate\Console\Scheduling\Schedule;

class NotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email to users';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Schedule $schedule)
    {

        // $schedulers     = EmailScheduler::where("start", Carbon::now()->format('Y-m-d H:i'))->get();

        $schedulers     = EmailScheduler::where("start", "<=" ,Carbon::now()->format('Y-m-d H:i'))->get();
       

        foreach ($schedulers as $scheduler) {

          switch ($scheduler->frequency){
            case "daily":
              if($scheduler->time != Carbon::now()->format('H:i')){
                return false;
              }
              break;
            case "weekly":
              if($scheduler->time != Carbon::now()->format('H:i') && $scheduler->day != Carbon::now()->format('l')){
                return false;
              }
              break;
            case "monthly":
              if($scheduler->time != Carbon::now()->format('H:i') && $scheduler->day != Carbon::now()->format('j')){
                return false;
              }
              break;

            default:
              return false;
            
          }
         

          if($scheduler->category_id){
            $customers      = Customer::where("category_id", $scheduler->category_id)->get();
          } else {
            $customers      = Customer::where("customer_type_id", $scheduler->customer_type_id)->get();
          }

          foreach ($customers as $customer) {
            
            $data = array(
              'id' => $customer->id,
              'getEmail' => $customer->email,
              'name' => $customer->name,
              'phone' => $customer->phone,
              'subject' => $scheduler->subject,
              'description' => $scheduler->description
            );

            Mail::send('dashboard.blasting.template', $data, function ($mail) use ($customer, $scheduler) {
              $mail->to($customer->email, 'no-reply')
                ->subject($scheduler->subject);
              $mail->from('dudu@hanara.id', 'Hanara Sentra Teknologi');
            });

          } 
 
        }

        echo "operation done";
    }
}


