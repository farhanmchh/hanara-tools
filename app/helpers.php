<?php

function romanNumber($value){
  switch ($value) {
    case 1:
      $result = 'I';
      break;
    case 2:
      $result = 'II';
      break;
    case 3:
      $result = 'III';
      break;
    case 4:
      $result = 'IV';
      break;
    case 5:
      $result = 'V';
      break;
    case 6:
      $result = 'VI';
      break;
    case 7:
      $result = 'VII';
      break;
    case 8:
      $result = 'VIII';
      break;
    case 9:
      $result = 'IX';
      break;
    case 10:
      $result = 'X';
      break;
    case 11:
      $result = 'XI';
      break;
    case 12:
      $result = 'XII';
      break;
    default:
      $result = null;
  }

  return $result;
}

function formatPrice($value){
  $result = null;
  switch('rupiah') {
    case 'rupiah' :
      $result = 'Rp. ' . number_format($value, 0, '.', '.');
      break;

    default :
      $result = number_format($value, 0, ',', '.');
      break;
  }

  return $result;
}
