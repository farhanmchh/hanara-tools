<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\QuotationController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DomainController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\EmailBlastController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectCategoryController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EmailSchedulerController;
use App\Http\Controllers\MomController;
use App\Http\Controllers\TncController;

use App\Models\SubCountry;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index'])->middleware('guest')->name('login');
Route::post('/', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::resource('quotation', QuotationController::class)->middleware('auth');
Route::resource('item', ItemController::class)->middleware('auth');
Route::resource('customer', CustomerController::class)->middleware('auth');
Route::resource('profile', ProfileController::class)->middleware('auth');
Route::resource('domain', DomainController::class)->middleware('auth');
Route::resource('vendor', VendorController::class)->middleware('auth');
Route::resource('email_blast', EmailBlastController::class)->middleware('auth');
Route::resource('email_scheduler', EmailSchedulerController::class)->middleware('auth');
Route::resource('category', CategoryController::class)->middleware('auth');
Route::resource('invoice', InvoiceController::class)->middleware('auth');
Route::resource('dashboard', DashboardController::class)->middleware('auth');
Route::resource('event', EventController::class)->middleware('auth');
Route::resource('employee', EmployeeController::class)->middleware('auth');
Route::resource('project', ProjectController::class)->middleware('auth');
Route::resource('project_category', ProjectCategoryController::class)->middleware('auth');
Route::resource('task', TaskController::class)->middleware('auth');
Route::resource('mom', MomController::class)->middleware('auth');
Route::resource('tnc', TncController::class)->middleware('auth');

Route::get('/email_blast/unsubscribe/{id}', [EmailBlastController::class, 'unsubscribe']);

Route::post('/category-project', [ProjectCategoryController::class, 'getProjectCategory']);
Route::get('/quotation/getCustomer/{id}', [QuotationController::class, 'getCustomer']);
Route::get('/quotation/getPrice/{id}', [QuotationController::class, 'getPrice']);
Route::get('/project/getCustomer/{id}', [ProjectController::class, 'getCustomer']);
Route::get('/print_quotation/{id}', [QuotationController::class, 'printPdf']);
Route::get('/quotation/email/{id}', [QuotationController::class, 'email']);
Route::get('/mom/email/{id}', [MomController::class, 'email']);
Route::get('/print_mom/{id}', [MomController::class, 'printPdf']);


Route::get('/getProvinces', function () {
  return response()->json(SubCountry::whereRaw('length(code) = 2')->orderBy('name')->get());
  
});

Route::get('/getProvince/{id}', function ($id) {
  return response()->json(SubCountry::where('code', $id)->first());
});

Route::get('/getCities/{id}', function ($id) {
  $data = SubCountry::whereRaw('length(code) = 5')
    ->where('code', 'like', $id . '%')
    ->orderBy('name')->get();
  return response()->json($data);
});

Route::get('/getCity/{id}', function ($id) {
  return response()->json(SubCountry::where('code', $id)->first());
});
