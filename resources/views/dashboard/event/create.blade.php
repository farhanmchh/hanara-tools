@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2>Add New Event</h2>
    </div>

    <a href="/event" class="btn btn-danger btn-sm mb-3">
        <i class="fas fa-times-circle"></i> Cancel
    </a>

    <form action="/event" method="post">
        <div class="row">
            <div class="col-md-6">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Event Name</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
                        value="{{ old('name') }}">
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <input type="text" name="description" class="form-control @error('description') is-invalid @enderror"
                        id="description" value="{{ old('description') }}"
                        placeholder="ex: Webinar Bersama Hanara Sentra Teknologi">
                    @error('description')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Date</label>
                    <input type="date" name="date" class="form-control text-center @error('date') is-invalid @enderror" value="{{ old('date') }}">
                    @error('date')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                    </div>
                </div>
                <div class="col-md-4 border-end border-2">
                    <div class="mb-3">
                        <label for="status" class="form-label">Status</label>
                        <select class="select-picker form-select" name="status" id="status" style="width: 100%">
                            <option>Incoming</option>
                            <option>Not Passed</option>
                            <option>Ongoing</option>
                            <option>Passed</option>
                            <option>Skipped</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 d-grid mt-3 ms-1">
                        <button type="submit" class="btn btn-primary">
                            <i class="fas fa-plus"></i> Add
                        </button>
                    </div>
                </div>
    </form>

@endsection
