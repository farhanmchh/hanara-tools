@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2 text-center">Edit Event Current Status</h1>
</div>

<a href="/event" class="btn btn-danger btn-sm mb-3">
    <i class="fas fa-times-circle"></i> Cancel
</a>

<form action="/event/{{ $event->id }}" method="POST">
@method('PATCH')
@csrf`
<div class="row">
  <div class="col-md-6">
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Event Name</label>
      <div class="col-sm-9">
        <input type="text" name="name" class="form-control text-center @error('name') is-invalid @enderror" value="{{ $event->name }}">
      </div>
      @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Description</label>
      <div class="col-sm-9">
        <input type="text" name="description" class="form-control text-center @error('description') is-invalid @enderror"
          value="{{ $event->description }}">
      </div>
    </div>
    <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">Date</label>
        <div class="col-sm-9">
            <input type="date" name="date"class="form-control form-control-sm text-center @error('date') is-invalid @enderror"
              value="{{ $event->date }}">
        </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Status</label>
      <div class="col-sm-9">
        <select class="select-picker form-select text-center" name="status" id="status" style="width: 100%">
          <option value="{{ $event->status }}" selected>{{ $event->status }}</option>
          <option value="Incoming" @if($event->status == 'Incoming') selected @endif>Incoming</option>
          <option value="Not Passed" @if($event->status == 'Not Passed') selected @endif>Not Passed</option>
          <option value="Ongoing" @if($event->status == 'Ongoing') selected @endif>Ongoing</option>
          <option value="Passed" @if($event->status == 'Passed') selected @endif>Passed</option>
          <option value="Skipped" @if($event->status == 'Skipped') selected @endif>Skipped</option>
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 d-grid mt-3 ms-1">
      <button type="submit" class="btn btn-warning">
          <i class="fas fa-paper-plane"></i> Update
      </button>
    </div>
  </div>
</form>
@endsection
