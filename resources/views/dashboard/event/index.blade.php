@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Events</h1>
    </div>

    <a href="/event/create" class="btn btn-primary mb-3">
        <i class="fas fa-plus"></i> Add New Event
    </a>
    <div class="row">
        <div class="container my-2">
            <div class="my-4">
                <form action="{{ route('event.index') }}" method="GET">
                    {{-- {{search by name or date}} --}}
                    <div class="my-4">
                        <div class="form-group">
                            <div class="row input">
                                <div class="col-md-9">
                                    <input class="form-control col-lg-10 col-sm-12" type="text" name="search"
                                        placeholder="Name or Date (Meetings Or 2021-09-15) ..."
                                        value="@isset($_GET['search']){{ $_GET['search'] }}@endisset" autocomplete="off">
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                {{-- {{end of search by name or date}} --}}

                {{-- {{search by date range}} --}}
                <div class="row input-daterange">
                    <div class="col-md-3">
                        <input type="date" class="form-control" name="start_date"
                            value='{{ app('request')->input('start_date') }}'>
                    </div>
                    <div class="col-md-3">
                        <input type="date" class="form-control" name="end_date"
                            value='{{ app('request')->input('end_date') }}'>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        <button type="button" class="btn btn-default"><a href="{{ route('event.index') }}">
                                <i class="fa fa-redo-alt"></i></a></button>
                    </div>
                </div>
                {{-- {{end of search by date range}} --}}
                </form>
            </div>
        </div>


        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                <strong>{{ session('success') }}</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif

        <div class="row">
            <div class="container my-4">
                <div class="table-responsive">
                    <table class="table table-striped table-sm text-center">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Events Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($event as $value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->description }}</td>
                                    <td>{{ $value->date }}</td>
                                    <td>{{ $value->status }}</td>
                                    <td>
                                        <a href="/event/{{ $value->id }}" class="btn btn-info btn-sm"><i
                                                class="far fa-eye"></i></a>
                                        <a href="/event/{{ $value->id }}/edit" class="btn btn-warning btn-sm"><i
                                                class="far fa-edit"></i></a>
                                        <form action="/event/{{ $value->id }}" method="POST" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-sm"
                                                onclick="return confirm('Are you sure?')"><i
                                                    class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    @endsection
