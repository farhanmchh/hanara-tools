@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2>Create a New Project</h2>
    </div>

    <a href="/employee" class="btn btn-danger btn-sm mb-3">
        <i class="fas fa-times-circle"></i> Cancel
    </a>

    <form action="/project" method="post">
        <div class="row">
            <div class="col-md-6">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Project Name</label>
                    <input type="text" name="name" class="form-control text-center  @error('name') is-invalid @enderror" id="name"
                    value="{{ old('name') }}" autocomplete="off">
                @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
                </div>    
                <div class="mb-3">
                    <label for="description" class="form-label">Client</label>
                    <select name="customer_id" id="to"
                    class="form-select form-select-sm text-center @error('customer_id') is-invalid @enderror">
                    <option></option>
                    @foreach ($customers as $customer)
                        @if (old('customer_id') == $customer->id)
                            <option value="{{ $customer->id }}" selected>{{ $customer->name }}</option>
                        @else
                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                        @endif
                    @endforeach
                    </select>
                       @error('customer_id')
                          <div class="invalid-feedback">
                               {{ $message }}
                           </div>
                       @enderror
                    </div>
                <div class="mb-3">
                    <label for="attn" class="form-label">Attn :</label>
                        <input type="text" class="form-control text-center" id="attn">
                </div> 
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">   
                <div class="mb-3">
                    <label for="description" class="form-label">Deadline</label>
                    <input type="date" name="deadline" class="form-control text-center @error('deadline') is-invalid @enderror" value="{{ old('deadline') }}">
                    @error('deadline')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label for="description" class="col-sm-2 col-form-label fw-bolder">Description:</label>
                    </div>
                    <div class="col-md-9">
                        <textarea style="height: 20vh;" name="description" class="form-control form-control-sm @error('description') is-invalid @enderror" value="{{ old('name') }}" autocomplete="off"></textarea>
                        @error('description')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" style="float: right;" class="form-control btn btn-primary my-4">
                            <i class="fas fa-plus"></i> Create
                        </button>
                    </div>
                </div>      
    </form>
    

    
    {{-- Input Customer --}}
    <script>
        const to = document.querySelector('#to');
        const attn = document.querySelector('#attn');

        fetch(`/project/getCustomer/${to.value}`)
            .then(response => response.json())
            .then(data => attn.value = data[0]['pic_name'])

        to.addEventListener('change', function() {
            fetch(`/project/getCustomer/${to.value}`)
                .then(response => response.json())
                .then(data => attn.value = data[0]['pic_name'])
        });
    </script>

@endsection
