@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Create Schedule</h2>
</div>

<a href="/email_scheduler" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
             @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
             @endforeach
        </ul>
    </div>
@endif


<form action="/email_scheduler" method="POST">
    @csrf
    <div class="row">
      <div class="col-md-12">
          <div class="row">
          <div class="form-group">
            <label for="from" class="col-sm-2 col-form-label fw-bolder">From</label>
            <div class="col-sm-12">
              <input type="text" name="from" class="form-control form-control text-center" value="{{ $company->name }}" readonly>
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="form-group">
            <label for="to" class="col-sm-2 col-form-label fw-bolder fw-bolder">Category</label>
            <div class="col-sm-12">
              <select name="category" class="form-select form-select">
                <option selected disabled>Choose Category</option>
                  <optgroup label="All Category">
                    @foreach ($customer_type as $tipe_customer)
                    <option value="{{ $tipe_customer->name }}">{{ $tipe_customer->name }}</option>
                    @endforeach
                  </optgroup>
                  <optgroup label="Category">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>
                <optgroup>
              @error('category')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
              @enderror
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="mb-3 row">
          <div class="form-group">
          <label for="subject" class="col-sm-2 col-form-label fw-bolder">Subject</label>
          <div class="col-sm-12">
            <input type="text" name="subject" class="form-control" value="{{ $edit->subject }}" required autocomplete="off"> 
          </div>
          </div>
        </div>
        <div class="mb-3 row">
          <div class="form-group">
            <label for="description" class="col-sm-2 col-form-label fw-bolder">Description</label>
            <div class="col-sm-12">
              <textarea type="text" id="editor1" rows="6" name="description" class="form-control" required autocomplete="off">{{ $edit->description }}</textarea>
            </div>
          </div>
          </div>
        </div>
    </div>

    <div class="row mb-3">
      <div class="col">
        <div class="form-group">
          <label for="input" class="col-sm-2 col-form-label fw-bolder">Start</label>
          <input id="input" name="start" width="100%" class="form-control" placeholder="Start date" value="{{ old('start') }}" required autocomplete="off">
        </div>
      </div>
      <div class="col">
        <div class="form-group">
          <label for="input2" class="col-sm-2 col-form-label fw-bolder">End</label>
          <input id="input2" name="end" width="100%" class="form-control" placeholder="End date" value="{{ old('end') }}" required autocomplete="off">
        </div>
      </div>
    </div>

    <div class="form-group mb-5">
      <label for="input" class="col-sm-2 col-form-label fw-bolder">Frequency</label>
      <select class="form-select" aria-label="Default select example" name="frequency" id="frequency" onChange="opsi(this)" required>
        <option selected disabled>Choose One</option>
        <option value="daily">Daily</option>
        <option value="weekly">Weekly</option>
        <option value="monthly">Monthly</option>
      </select>
    </div>

    <div class="row">
      <div class="col">
        <div class="mb-3">
          <label for="time" class="form-label">Time</label>
          <input type="time" name="time" class="form-control @error('time') is-invalid @enderror" id="time" disabled
          value="{{ old('time') }}">
          @error('time')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="col">
        <div class="mb-3">
          <label for="day" class="form-label">day</label>
          <select class="form-select" aria-label="Default select example" name="day" id="day" disabled>
            <option selected disabled>Choose One</option>
            <option value="Sunday">Sunday</option>
            <option value="Monday">Monday</option>
            <option value="Tuesday">Tuesday</option>
            <option value="Wednesday">Wednesday</option>
            <option value="Thrusday">Thrusday</option>
            <option value="Friday">Friday</option>
            <option value="Saturday">Saturday</option>
          </select>
        </div>
      </div>
      <div class="col">
         <div class="mb-3">
      <label for="date" class="form-label">date</label>
      <input type="text" name="date" class="form-control @error('date') is-invalid @enderror" id="date" disabled
      value="{{ old('date') }}">
      @error('date')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md d-grid">
          <button type="submit" class="btn btn-primary my-4">
            <i class="fas fa-plus"></i> Create
          </button>
        </div>
    </div>
</form>
<script>
  CKEDITOR.replace( 'editor1', {
        language: 'en',
        uiColor: '#9AB8F3'
    });

   $('#input').datetimepicker({ uiLibrary: 'bootstrap4', modal: true, footer: true });
   $('#input2').datetimepicker({ uiLibrary: 'bootstrap4', modal: true, footer: true });

   function opsi(value){
    var frequency = $("#frequency").val();

    if(frequency == "daily"){
      document.getElementById("time").disabled = false;
    } else {
      document.getElementById("time").disabled = true;
    }

    if(frequency == "weekly"){
      document.getElementById("day").disabled = false;
      document.getElementById("time").disabled = false;
    } else {
      document.getElementById("day").disabled = true;
    }

    if(frequency == "monthly"){
      document.getElementById("date").disabled = false;
      document.getElementById("time").disabled = false;
    } else {
      document.getElementById("date").disabled = true;
    }

    }
</script>    
@endsection
