
@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Email Scheduler</h1>
</div>

<a href="/email_scheduler/create" class="btn btn-primary mb-3">
	<i class="fas fa-plus"></i> Create Schedule
</a>

@if (session('success'))
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<strong>{{ session('success') }}</strong>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
@endif

<div class="table-responsive">
	<table class="table table-striped table-sm">
		<thead>
			<tr>
				<th scope="col">Category</th>
				<th scope="col">Subject</th>
                {{-- <th scope="col" width="10%">Delivered</th> --}}
				<th scope="col">Frequency</th>
				<th scope="col">Day</th>
				<th scope="col">Every Hour</th>
				<th scope="col">Start_at</th>
				<th scope="col">End_at</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($email_scheduler as $item)
			<tr>
				<form action="/email/sendEmail" class="d-inline" method="POST">
					@csrf
				<td>
					@if ($item->category == null)
					{{ $item->customerType->name ?? $item->category->name }}
					@else
					{{ $item->category->name ?? $item->customerType->name  }}
					@endif
				</td>
				<td>{{ $item->subject }}</td>
                {{-- <td>
                    <input type="text" class="form-control form-control-sm bg-transparent border-0" value="{{ $item->delivered == 1 ? 'Yes' : 'No' }}" name="description" readonly >
                </td> --}}
				<td>{{ $item->frequency }}</td>
				<td>{{ $item->day ?? "-" }}</td>
				<td>{{ $item->time ?? "null" }}</td>
				<td>{{ date('d M Y - h:i', strtotime($item->start))}}</td>
				<td>{{ date('d M Y - h:i', strtotime($item->end))}}</td>
				<td>
                </form>
					<a href="/email_scheduler/{{ $item->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
					<a href="/email_scheduler/{{ $item->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
					<form action="/email_scheduler/{{ $item->id }}" method="POST" class="d-inline">
						@method('delete')
						@csrf
						<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection