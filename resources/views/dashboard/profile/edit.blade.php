@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 text-center border-bottom">
  <h1 class="h2">Edit Company Profile</h1>
</div>

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
  <strong>{{ session('success') }}</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<a href="/profile" class="btn btn-danger btn-sm">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<form action="/profile/{{ $company->id }}" method="POST">
@method('put') 
@csrf
<div class="row justify-content-center">
  <div class="col-md text-center">
    <img src="/img/hanara-logo.png" class="img-thumbnail rounded-circle shadow mb-3" width="15%" alt="{{ $company->name }}">
    <input type="text" name="name" class="form-control text-center @error('name') is-invalid @enderror" 
    value="{{ ((old('name')) ? old('name') : $company->name) }}">
    @error('name')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
    @enderror
  </div>
</div>

<div class="row">
  <div class="col-md-6 mt-5 mb-3 border-end border-2">
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">PIC</label>
      <div class="col-sm-9">
        <input type="text" name="pic" class="form-control form-control-sm @error('pic') is-invalid @enderror"
        value="{{ ((old('pic')) ? old('pic') : $company->pic) }}">
        @error('pic')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Role</label>
      <div class="col-sm-9">
        <input type="text" name="role" class="form-control form-control-sm @error('role') is-invalid @enderror" 
        value="{{ ((old('role')) ? old('role') : $company->role) }}">
        @error('role')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Phone</label>
      <div class="col-sm-9">
        <input type="text" name="phone" class="form-control form-control-sm @error('phone') is-invalid @enderror" 
        value="{{ ((old('phone')) ? old('phone') : $company->phone) }}">
        @error('phone')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">E-mail</label>
      <div class="col-sm-9">
        <input type="text" name="email" class="form-control form-control-sm @error('email') is-invalid @enderror" 
        value="{{ ((old('email')) ? old('email') : $company->email) }}">
        @error('email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Website</label>
      <div class="col-sm-9">
        <input type="text" name="website" class="form-control form-control-sm @error('website') is-invalid @enderror" 
        value="{{ ((old('website')) ? old('website') : $company->website) }}">
        @error('website')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
      </div>
    </div>
  </div>
  <div class="col-md-6 mt-5">
    <div class="mb-3 row text-center">
      <label class="form-label fw-bolder">Address</label>
      <textarea name="address" class="form-control form-control-sm pb-2 px-2 mx-2 @error('address') is-invalid @enderror">
        {{ ((old('address')) ? old('address') : $company->address) }}
      </textarea>
      @error('address')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
    @enderror
    </div>
    <div class="mb-3 row">
      <div class="mb-3">
        <label class="form-label">Description</label>
        <input id="tnc" type="hidden" name="tnc" value="{{ ((old('tnc')) ? old('tnc') : $company->tnc) }}">
        <trix-editor input="tnc" class="@error('tnc') form-control is-invalid @enderror"></trix-editor>
        @error('tnc')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>
  </div>
</div>

<div class="row justify-content-center">
  <div class="col-md-6 mb-3">
    <div class="d-grid">
      <button type="submit" class="btn btn-warning px-5 py-1"> 
        <i class="fas fa-paper-plane"></i> Update
      </button>
    </div>
  </div>
</div>

</form>



@endsection