@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 text-center border-bottom">
  <h1 class="h2">Company Profile</h1>
</div>

@if ($total_company == 0)
<div class="text-center mt-5">
  <i class="fad fa-user-slash h1 text-danger"></i>
  <h4 class="text-danger">Profile is Empty</h4>
  <a href="/profile/create" class="btn btn-primary">Create Your Company Profile</a>
</div>
@endif

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
  <strong>{{ session('success') }}</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<a href="/profile/{{ $company->id }}/edit" class="btn btn-warning btn-sm">
  <i class="fas fa-edit"></i> Edit
</a>

<div class="row justify-content-center">
  <div class="col-md text-center">
    <img src="/img/hanara-logo.png" class="img-thumbnail rounded-circle shadow mb-3" width="15%" alt="{{ $company->name }}">
    <h4 class="text-center">{{ $company->name }}</h4>
  </div>
</div>

<div class="row">
  <div class="col-md-6 mt-5 border-end border-2 mb-3">
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">PIC</label>
      <div class="col-sm-9">
        <p class="border-bottom fs-6 pb-2 ps-2">{{ $company->pic }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Role</label>
      <div class="col-sm-9">
        <p class="border-bottom fs-6 pb-2 ps-2">{{ $company->role }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Phone</label>
      <div class="col-sm-9">
        <p class="border-bottom fs-6 pb-2 ps-2">{{ $company->phone }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">E-mail</label>
      <div class="col-sm-9">
        <p class="border-bottom fs-6 pb-2 ps-2">{{ $company->email }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Website</label>
      <div class="col-sm-9">
        <p class="border-bottom fs-6 pb-2 ps-2">{{ $company->website }}</p>
      </div>
    </div>
  </div>
  <div class="col-md-6 mt-5">
    <div class="mb-3 row text-center">
      <label class="form-label fw-bolder">Address</label>
      <p class="border-bottom fs-6 pb-2 px-2 mx-2">{{ $company->address }}</p>
    </div>
  </div>
</div>

@endsection