@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 text-center border-bottom">
  <h1 class="h2">Create Company Profile</h1>
</div>

<form action="/profile" method="post">
@csrf

<div class="row">
  <div class="col-md-4">
    <div class="mb-3">
      <label for="name" class="form-label">Company Name</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}">
      @error('name')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="pic" class="form-label">PIC</label>
      <input type="text" name="pic" class="form-control @error('pic') is-invalid @enderror" id="pic" value="{{ old('pic') }}">
      @error('pic')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="role" class="form-label">Role</label>
      <input type="text" name="role" class="form-control @error('role') is-invalid @enderror" id="role" value="{{ old('role') }}">
      @error('role')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    
  </div>
  <div class="col-md-4">
    <div class="mb-3">
      <label for="email" class="form-label">E-mail</label>
      <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}">
      @error('email')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="website" class="form-label">Website</label>
      <input type="text" name="website" class="form-control @error('website') is-invalid @enderror" id="website" value="{{ old('website') }}">
      @error('website')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="logo" class="form-label">Logo</label>
      <input type="text" name="logo" class="form-control @error('logo') is-invalid @enderror" id="logo" value="{{ old('logo') }}">
      @error('logo')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
  </div>
  <div class="col-md-4">
    <div class="mb-3">
      <label for="phone" class="form-label">Phone</label>
      <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ old('phone') }}">
      @error('phone')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="address" class="form-label">Address</label>
      <textarea name="address" class="form-control @error('address') is-invalid @enderror" id="address" rows="4" value="{{ old('address') }}"></textarea>
      @error('address')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
  </div>
</div>

<div class="row align-items-end">
  <div class="col-md-8">
    <div class="mb-3">
      <label for="tnc" class="form-label">Term & Condition</label>
      <input type="hidden" name="tnc" class="form-control @error('tnc') is-invalid @enderror" id="tnc" value="{{ old('tnc') }}">
      <trix-editor input="tnc"></trix-editor>
      @error('tnc')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
  </div>
  <div class="col-md-4">
    <div class="mb-3 d-grid">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
</div>

</form>

@endsection