@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Categories</h1>
</div>

<a href="/category/create" class="btn btn-primary mb-3">
  <i class="fas fa-plus"></i> Create New Category
</a>


<div class="col-md-6">
  @if (session('success'))
    <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
      <strong>{{ session('success') }}</strong>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  @endif
  <div class="table-responsive">
    <table class="table table-sm text-center">
      <thead>
        <tr>
          <th scope="col">No.</th>
          <th scope="col">Name</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($categories as $category)
        <tr class="name-container">
          <td>{{ $loop->iteration }}</td>
          <form method="POST" class="form">
            @csrf
            <td>
              {{ $category->name }}
              {{-- <input type="hidden" class="name-id" value="{{ $category->id }}">
              <input type="text" name="name" class="name form-control form-control-sm text-center border-0" disabled value="{{ $category->name }}"> --}}
            </td>
          </form>
          <td>
            <a href="/category/{{ $category->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
            <form action="/category/{{ $category->id }}" method="POST" class="d-inline">
              @method("DELETE")
              @csrf
              <button class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure?')"><i class="fas fa-trash"></i></button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

{{-- <script>
  $(document).on('dblclick', '.name', function() {
    let eachName = $(this).closest('.name-container');
    $.get(`/getCategory/${eachName.find('.name-id').val()}`, function(n) {
      eachName.find('.name').removeAttr('disabled');
      let data = eachName.find('.form').serialize();
      // console.log(data);
      eachName.find('.name').on('change', function() {
        $.ajax({
          type: 'POST',
          url: `/updateCategory/${eachName.find('.name-id').val()}`,
          data: data,
          success: function() {
            eachName.find('.name').attr('disabled').load('/category/1');
          }
        })
      });
    });
  });
</script> --}}

@endsection