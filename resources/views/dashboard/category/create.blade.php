@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Create Category</h1>
</div>

<a href="/category" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>
<div class="col-md-6">
  <form action="/category" method="POST">
    @csrf
    <div class="mb-3">
      <label for="name" class="form-label">Name</label>
      <input type="text" name="name" id="name" class="form-control @error ('name') is-invalid @enderror" value="{{ old('name') }}" autofocus autocomplete="off">
      @error('name')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
      @enderror
    </div>
    <button class="btn btn-primary" type="submit">Create</button>
  </form>
</div>
    
@endsection