@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h2>Add New Vendor</h2>
</div>

<a href="/vendor" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<form action="/vendor" method="post">
  <div class="row">

    <div class="col-md-4 border-end border-2">
      @csrf
      <div class="mb-3">
        <label for="name" class="form-label">* Company Name</label>
        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}">
        @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="phone" class="form-label">* Phone</label>
        <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ old('phone') }}">
        @error('phone')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="email" class="form-label">* E-mail</label>
        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}"
        placeholder="ex: hanara@gmail.com">
        @error('email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="province" class="form-label">* Province</label>
        <select class="select-picker form-select" name="province" id="province" style="width: 100%">     
        </select>
      </div>
      <div class="mb-3">
        <label for="city" class="form-label">* City</label>
        <select class="select-picker form-select" name="city" id="city" style="width: 100%">    
        </select>
      </div>
    </div>

    <div class="col-md-4 border-end border-2">
      <div class="mb-3">
        <label for="website" class="form-label">Website</label>
        <input type="text" name="website" class="form-control @error('website') is-invalid @enderror" id="website" value="{{ old('website') }}"
        placeholder="ex: https://hanara.id/">
        @error('website')
        <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="pic_name" class="form-label">PIC Name</label>
        <input type="text" name="pic_name" class="form-control @error('pic_name') is-invalid @enderror" id="pic_name" value="{{ old('pic_name') }}">
        @error('pic_name')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="mb-3">
          <label for="pic_phone" class="form-label">PIC Phone</label>
          <input type="text" name="pic_phone" class="form-control @error('pic_phone') is-invalid @enderror" id="pic_phone" value="{{ old('pic_phone') }}">
          @error('pic_phone')
              <div class="invalid-feedback">
              {{ $message }}
              </div>
          @enderror
      </div>
      <div class="mb-3">
          <label for="pic_email" class="form-label">PIC E-mail</label>
          <input type="text" name="pic_email" class="form-control @error('pic_email') is-invalid @enderror" id="pic_email" value="{{ old('pic_email') }}"
          placeholder="ex: hanara@gmail.com">
          @error('pic_email')
              <div class="invalid-feedback">
              {{ $message }}
              </div>
          @enderror
      </div>
    </div>

    <div class="col-md-4">
      <div class="mb-3">
        <label for="address" class="form-label">* Address</label>
        <textarea type="text" name="address" class="form-control @error('address') is-invalid @enderror" id="address" value="{{ old('address') }}" rows="11"></textarea>
        @error('address')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
    </div>
  </div>
  
  <div class="row justify-content-center">
    <div class="col-md-8 d-grid my-4">
      <button type="submit" class="btn btn-primary">
        <i class="fas fa-plus"></i> Add
      </button>
    </div>
  </div>
</form>

<script src="http://api.iksgroup.co.id/apijs/lokasiapi.js"></script>

{{-- API Province, City --}}
<script>
  const province = document.querySelector('#province');  
  const city = document.querySelector('#city');

  fetch(`/getProvinces`)
    .then(response => response.json())
    .then(provinces => {
      let provinceOption = '<option>- Select Province -</option>';
      provinces.forEach(p => {
        provinceOption += `<option value="${p.code}">${p.name}</option>`
        province.innerHTML = provinceOption;
      })

      province.addEventListener('change', function() {
        fetch(`/getCities/${this.value}`)
          .then(response => response.json())
          .then(cities => {
            let cityOption = '<option>- Select City -</option>';
            cities.forEach(c => {
              cityOption += `<option value="${c.code}">${c.name}</option>`
              city.innerHTML = cityOption;
            })
          })
      })
    })
</script>

{{-- Select2 --}}
{{-- <script>
  $(document).ready(function() {
    $('.select-picker').select2({
      placeholder: "- Select Company -",
      theme: "bootstrap-5",
      selectionCssClass: "select2--small", // For Select2 v4.1
      dropdownCssClass: "select2--small",
    });
  });
</script> --}}

@endsection