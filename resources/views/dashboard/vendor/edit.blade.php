@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Edit Vendor</h1>
</div>

<a href="/vendor" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<form action="/vendor/{{ $vendor->id }}" method="POST">
  @method('PATCH')
  @csrf

  <div class="row ">

    <div class="col-md-6">
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">Company Name</label>
        <div class="col-sm-9">
          <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
          value="{{ $vendor->name }}">
        </div>
        @error('name')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">Phone</label>
        <div class="col-sm-9">
          <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror"
          value="{{ $vendor->phone }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">E-mail</label>
        <div class="col-sm-9">
          <input type="text" name="email" class="form-control @error('email') is-invalid @enderror"
          value="{{ $vendor->email }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">Address</label>
        <div class="col-sm-9">
          <textarea name="address" class="form-control @error('address') is-invalid @enderror" value="" rows="8">{{ $vendor->address }}</textarea>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="mb-3 row">
        <label for="province" class="col-sm-3 form-label fw-bolder">Province</label>
        <div class="col-sm-9">
          <select class="select-picker form-select" name="province" id="province" style="width: 100%">
            <option value="{{ $vendor->province }}"></option>
          </select>
        </div>
      </div>
      <div class="mb-3 row">
        <label for="city" class="col-sm-3 form-label fw-bolder">City</label>
        <div class="col-sm-9">
          <select class="select-picker form-select" name="city" id="city" style="width: 100%">
            <option value="{{ $vendor->city }}"></option>
          </select>
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">Website</label>
        <div class="col-sm-9">
          <input type="text" name="website" class="form-control @error('website') is-invalid @enderror"
          value="{{ $vendor->website }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">PIC Name</label>
        <div class="col-sm-9">
          <input type="text" name="pic_name" class="form-control @error('pic_name') is-invalid @enderror"
          value="{{ $vendor->pic_name }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">PIC Phone</label>
        <div class="col-sm-9">
          <input type="text" name="pic_phone" class="form-control @error('pic_phone') is-invalid @enderror"
          value="{{ $vendor->pic_phone }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 col-form-label fw-bolder">PIC E-mail</label>
        <div class="col-sm-9">
          <input type="text" name="pic_email" class="form-control @error('pic_email') is-invalid @enderror"
          value="{{ $vendor->pic_email }}">
        </div>
      </div>
    </div>

  </div>

  <div class="row justify-content-center">
    <div class="col-md-6 mb-3">
      <div class="d-grid">
        <button type="submit" class="btn btn-warning">
          <i class="fas fa-paper-plane"></i> Update
        </button>
      </div>
    </div>
  </div>

</form>

{{-- API Province, City --}}
<script>
  const province = document.querySelector('#province');  
  const city = document.querySelector('#city');

  fetch(`/getProvinces`)
    .then(response => response.json())
    .then(provinces => {
      let provinceOption;

      let p = provinces.findIndex(element => {
        if(element.code == province.value) {
          return true;
        }
      })

      for (let i = 0; i < provinces.length - 1; i++) {
        if (i == p) {
          provinceOption += `<option value="${provinces[p]['code']}" selected>${provinces[p]['name']}</option>`;
        }
        if (i != p) {
          provinceOption += `<option value="${provinces[i]['code']}">${provinces[i]['name']}</option>`;
        }
        
        province.innerHTML = provinceOption;
      }
    })
    
    fetch(`/getCities/${province.value}`)
    .then(response => response.json())
    .then(cities => {
      let cityOption;
      
      let c = cities.findIndex(element => {
        if (element.code == city.value) {
          return true;
        }
      })
      
      for (let i = 0; i < cities.length - 1; i++) {
        if (i == c) {
          cityOption += `<option value="${cities[c]['code']}" selected>${cities[c]['name']}</option>`;
        }
        if (i != c) {
          cityOption += `<option value="${cities[i]['code']}">${cities[i]['name']}</option>`;
        }
        city.innerHTML = cityOption;
      }
    })

  province.addEventListener('change', function() {
    fetch(`/getCities/${this.value}`)
      .then(response => response.json())
      .then(cities => {
        let cityOption = '<option>- Select City -</option>';
        cities.forEach(c => {
          cityOption += `<option value="${c.code}">${c.name}</option>`
          city.innerHTML = cityOption;
        })
      })
  })
</script>

@endsection