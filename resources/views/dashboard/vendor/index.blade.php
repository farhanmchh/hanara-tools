@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2 text-center">Vendors</h1>
</div>

<div class="table-responsive">
	<a href="/vendor/create" class="btn btn-primary mb-3">
		<i class="fas fa-plus"></i> Add New Vendor
	</a>

	@if (session('success'))
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<strong>{{ session('success') }}</strong>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
	@endif

	<table class="table table-striped table-sm text-center">
		<thead>
			<tr>
				<th scope="col">No</th>
				<th scope="col">Company name</th>
				<th scope="col">Phone</th>
				<th scope="col">E-mail</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($vendors as $vendor)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $vendor->name }}</td>
				<td>{{ $vendor->phone }}</td>
				<td>{{ $vendor->email }}</td>
				<td>
					<a href="/vendor/{{ $vendor->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
					<a href="/vendor/{{ $vendor->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
					<form action="/vendor/{{ $vendor->id }}" method="POST" class="d-inline">
						@method('delete')
						@csrf
						<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this vendor?')"><i class="fas fa-trash"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection