@extends('dashboard.layouts.main')

@section('content')
  
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Detail Vendor</h1>
</div>

<div class="mb-4">
  <a href="/vendor" class="btn btn-info btn-sm">
    <i class="fas fa-angle-left"></i>
  </a>
  <a href="/vendor/{{ $vendor->id }}/edit" class="btn btn-warning btn-sm">
    <i class="fas fa-edit"></i> Edit
  </a>
  <form action="/vendor/{{ $vendor->id }}" method="POST" class="d-inline">
    @method('delete')
    @csrf
    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">
      <i class="fas fa-trash"></i> Delete
    </button>
  </form>
</div>

@if (session('success'))
  <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
  
<div class="row">
  <div class="col-md-6 border-end border-2">
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Company Name</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ $vendor->name }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Phone</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ $vendor->phone }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">E-mail</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ $vendor->email }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Province</label>
      <div class="col-sm-9">
        <input type="hidden" id="provinceId" value="{{ $vendor->province }}">
        <p class="fs-6 border-bottom px-2 py-1" id="province"></p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">City</label>
      <div class="col-sm-9">
        <input type="hidden" id="cityId" value="{{ $vendor->city }}">
        <p class="fs-6 border-bottom px-2 py-1" id="city"></p>
      </div>
    </div>
  </div>
  
  <div class="col-md-6">
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Address</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ $vendor->address }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">Website</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ ($vendor->website) ? $vendor->website : '(Not yet filled)' }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">PIC Name</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ ($vendor->pic_name) ? $vendor->pic_name : '(Not yet filled)' }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">PIC Phone</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ ($vendor->pic_phone) ? $vendor->pic_phone : '(Not yet filled)' }}</p>
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-3 col-form-label fw-bolder">PIC E-mail</label>
      <div class="col-sm-9">
        <p class="fs-6 border-bottom px-2 py-1">{{ ($vendor->pic_email) ? $vendor->pic_email : '(Not yet filled)' }}</p>
      </div>
    </div>
  </div>
</div>
  
<script>
  const province = document.querySelector('#province');
  const provinceId = document.querySelector('#provinceId');
  const city = document.querySelector('#city');
  const cityId = document.querySelector('#cityId');
  
  fetch(`/getProvince/${provinceId.value}`)
    .then(response => response.json())
    .then(p => province.innerHTML = p.name)
  
  fetch(`/getCity/${cityId.value}`)
    .then(response => response.json())
    .then(c => city.innerHTML = c.name)
</script>

@endsection