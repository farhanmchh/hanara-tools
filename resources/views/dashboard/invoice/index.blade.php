@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Invoice</h1>
</div>

<a href="/invoice/create" class="btn btn-primary mb-3">
  <i class="fas fa-plus"></i> Create a New Invoice
</a>

@if (session('success'))
  <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

<div class="table-responsive">
  <table class="table table-striped table-sm text-center">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Customer Name</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($invoice as $invoice)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $invoice->name }}</td>
        <td>{{ $invoice->status }}</td>
        <td>
          <a href="/invoice/{{ $invoice->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
					<a href="/invoice/{{ $invoice->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
					<form action="/invoice/{{ $invoice->id }}" method="POST" class="d-inline">
						@method("delete")
						@csrf
						<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure to delete this invoice?')"><i class="fas fa-trash"></i></button>
					</form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection