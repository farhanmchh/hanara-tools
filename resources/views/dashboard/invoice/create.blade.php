@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Create a New Invoice</h2>
</div>

<a href="/invoice" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<div class="col-md-6">
  <form action="/invoice" method="post">
    @csrf
    <div class="mb-3">
      <label for="name" class="form-label">Invoice Name</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}" autofocus autocomplete="off">
      @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="status" class="form-label">Status</label>
      <input type="text" name="status" class="form-control @error('status') is-invalid @enderror" id="status" value="{{ old('status') }}" autocomplete="off">  
      @error('status')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label class="form-label">Description</label>
      <trix-editor input="x" class="@error('description') form-control is-invalid @enderror"></trix-editor>
      <input id="x" type="hidden" name="description">
      @error('description')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

@endsection