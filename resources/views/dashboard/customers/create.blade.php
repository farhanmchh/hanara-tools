@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h2>Add New Customer</h2>
</div>

<a href="/customer" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<form action="/customer" method="post">
  <div class="row">
    <div class="col-md-4 border-end border-2">
      @csrf
      <div class="mb-3">
        <label for="name" class="form-label">* Company Name</label>
        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}">
        @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="phone" class="form-label">* Phone</label>
        <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ old('phone') }}">
        @error('phone')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="email" class="form-label">* E-mail</label>
        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}"
        placeholder="ex: hanara@gmail.com">
        @error('email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="province" class="form-label">* Province</label>
        <select class="select-picker form-select @error('province') is-invalid @enderror" name="province" id="province" style="width: 100%">
        
        </select>
        @error('province')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        <input type="hidden" id="oldProvince" value="{{ old('province') }}">
      </div>
      <div class="mb-3">
        <label for="city" class="form-label">* City</label>
        <select class="select-picker form-select @error('city') is-invalid @enderror" name="city" id="city" style="width: 100%">
          
        </select>
        @error('city')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        <input type="hidden" id="oldCity" value="{{ old('city') }}">
      </div>
    </div>
    <div class="col-md-4 border-end border-2">
      <div class="mb-3">
        <label for="category_id" class="form-label">* Category</label>
        <select class="select-picker form-select @error('category_id') is-invalid @enderror" name="category_id" id="category_id" style="width: 100%">
          <option></option>
          @foreach ($categories as $category)
            @if (old('category_id') == $category->id)
              <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
            @else
              <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endif
          @endforeach
        </select>
        @error('category_id')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="type" class="form-label">* Type</label>
        <select name="type" id="type" class="form-select @error('type') is-invalid @enderror">
          <option></option>
          <option value="database only" @if(old('type') == 'database only') selected @endif>Database Only</option>
          <option value="one time customer" @if(old('type') == 'one time customer') selected @endif>One Time Customer</option>
          <option value="customer"  @if(old('type') == 'customer') selected @endif>Customer</option>
        </select>
        @error('type')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="pic_name" class="form-label">PIC Name</label>
        <input type="text" name="pic_name" class="form-control" id="pic_name" value="{{ old('pic_name') }}">
      </div>
      <div class="mb-3">
          <label for="pic_phone" class="form-label">PIC Phone</label>
          <input type="text" name="pic_phone" class="form-control" id="pic_phone" value="{{ old('pic_phone') }}">
      </div>
      <div class="mb-3">
          <label for="pic_email" class="form-label">PIC E-mail</label>
          <input type="text" name="pic_email" class="form-control" id="pic_email" value="{{ old('pic_email') }}"
          placeholder="ex: hanara@gmail.com">
      </div>
    </div>
    <div class="col-md-4">
      <div class="mb-3">
        <label for="website" class="form-label">Website</label>
        <input type="text" name="website" class="form-control" id="website" value="{{ old('website') }}"
        placeholder="ex: https://hanara.id/">
      </div>
      <div class="mb-3">
        <label for="address" class="form-label">* Address</label>
        <textarea type="text" name="address" class="form-control @error('address') is-invalid @enderror" id="address" value="{{ old('address') }}" rows="11"></textarea>
        @error('address')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-md-8 d-grid my-4">
      <button type="submit" class="btn btn-primary">
        <i class="fas fa-plus"></i> Add
      </button>
    </div>
  </div>
</form>

{{-- API Province, City --}}
<script>
  const province = document.querySelector('#province');  
  const city = document.querySelector('#city');
  const oldProvince = document.querySelector('#oldProvince');
  const oldCity = document.querySelector('#oldCity');

  fetch(`/getProvinces`) 
    .then(response => response.json())
    .then(provinces => {
      let provinceOption = '<option></option>';
      provinces.forEach(p => {
        provinceOption += `<option value="${p.code}">${p.name}</option>`
        province.innerHTML = provinceOption;
      })

      province.addEventListener('change', function() {
        fetch(`/getCities/${this.value}`)
          .then(response => response.json())
          .then(cities => {
            let cityOption = '<option></option>';
            cities.forEach(c => {
              city.addEventListener('change', function() {
                console.log(c.code)
              })
              cityOption += `<option value="${c.code}">${c.name}</option>`
              city.innerHTML = cityOption;
            })
          })
      })
    })

  if (oldProvince.value) {
    fetch(`/getProvinces`)
      .then(response => response.json())
      .then(provinces => {
        let provinceOption = '<option></option>';

        let p = provinces.findIndex(element => {
          if(element.code == oldProvince.value) {
            return true;
          }
        })
        for (let i = 0; i < provinces.length - 1; i++) {
          if (i == p) {
            provinceOption += `<option value="${provinces[p]['code']}" selected>${provinces[p]['name']}</option>`;
          } else {
            provinceOption += `<option value="${provinces[i]['code']}">${provinces[i]['name']}</option>`;
          }
          province.innerHTML = provinceOption;
        }
      })

    if (oldCity.value) {
      fetch(`/getCities/${oldProvince.value}`)
        .then(response => response.json())
        .then(cities => {
          let cityOption = '<option></option>';
          
          let c = cities.findIndex(element => {
            if (element.code == oldCity.value) {
              return true;
            }
          })
          
          for (let i = 0; i < cities.length - 1; i++) {
            if (i == c) {
              cityOption += `<option value="${cities[c]['code']}" selected>${cities[c]['name']}</option>`;
            } else {
              cityOption += `<option value="${cities[i]['code']}">${cities[i]['name']}</option>`;
            }
            city.innerHTML = cityOption;
          }
        })
    }
  }
</script>

{{-- Select2 --}}
{{-- <script>
  $(document).ready(function() {
    $('.select-picker').select2({
      placeholder: "- Select Company -",
      theme: "bootstrap-5",
      selectionCssClass: "select2--small", // For Select2 v4.1
      dropdownCssClass: "select2--small",
    });
  });
</script> --}}

@endsection