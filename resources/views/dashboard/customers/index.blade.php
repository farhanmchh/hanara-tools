@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Customers</h1>
    </div>

    <a href="/customer/create" class="btn btn-primary mb-3">
        <i class="fas fa-plus"></i> Add New Customer
    </a>

    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong>{{ session('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong>{{ session('error') }}</strong> <a href="/category/create">Click Here!</a>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-sm text-center">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Company name</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Category</th>
                    <th scope="col">Type</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->category->name ?? '' }}</td>
                        <td>{{ $customer->type }}</td>
                        <td>
                            <a href="/customer/{{ $customer->id }}" class="btn btn-info btn-sm"><i
                                    class="far fa-eye"></i></a>
                            <a href="/customer/{{ $customer->id }}/edit" class="btn btn-warning btn-sm"><i
                                    class="far fa-edit"></i></a>
                            <form action="/customer/{{ $customer->id }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
