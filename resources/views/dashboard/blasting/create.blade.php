@extends('dashboard.layouts.main')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Create Email Blast</h2>
</div>

<a href="/email_blast" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
             @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
             @endforeach
        </ul>
    </div>
@endif

@if (session('error'))
	<div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
		<strong>{{ session('error') }}</strong>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
@endif

<form action="/email_blast" method="POST">
    @csrf
    <div class="row">
      <div class="col-md-12">
          <div class="row">
          <div class="form-group">
            <label for="from" class="col-sm-2 col-form-label fw-bolder">From</label>
            <div class="col-sm-12">
              <input type="text" name="from" class="form-control form-control text-center" value="{{ $company->name }}" readonly>
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="form-group">
            <label for="to" class="col-sm-2 col-form-label fw-bolder fw-bolder">Category</label>
            <div class="col-sm-12">
              <select name="category" class="form-select form-select">
                <option selected disabled>Choose Category</option>
                  <optgroup label="All Category">
                    @foreach ($customer_type as $tipe_customer)
                    <option value="{{ $tipe_customer->name }}">{{ $tipe_customer->name }}</option>
                    @endforeach
                  </optgroup>
                  <optgroup label="Category">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>
                <optgroup>
              @error('category')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
              @enderror
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="mb-3 row">
          <div class="form-group">
          <label for="subject" class="col-sm-2 col-form-label fw-bolder">Subject</label>
          <div class="col-sm-12">
            <input type="text" name="subject" class="form-control" value="{{ old('subject') }}" required autocomplete="off"> 
          </div>
          </div>
        </div>
        <div class="mb-3 row">
          <div class="form-group">
            <label for="description" class="col-sm-2 col-form-label fw-bolder">Description</label>
            <div class="col-sm-12">
              <textarea id="editor1" type="text" rows="6" name="description" class="form-control" required autocomplete="off">{{ old('description') }}</textarea>
            </div>
          </div>
          </div>
        </div>
    </div>
    <div class="row mb-5">
      <div class="col">
        <div class="form-group">
          <label for="input" class="col-sm-2 col-form-label fw-bolder">Blast_at</label>
          <input id="input" name="blast_at" width="100%" class="form-control" value="{{ old('blast_at') }}" placeholder="Start date" required autocomplete="off" required> 
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md d-grid">
          <button type="submit" class="btn btn-primary my-4">
            <i class="fas fa-plus"></i> Create
          </button>
        </div>
    </div>
</form>

<script>
  CKEDITOR.replace( 'editor1', {
        language: 'en',
        uiColor: '#9AB8F3',
        filebrowserUploadUrl: '{{route('email_blast.store', ['_token' => csrf_token() ])}}',
        filebrowserUploadMethod: 'form'
  });

  $('#input').datetimepicker({ uiLibrary: 'bootstrap4', modal: true, footer: true });
</script>    
@endsection
