
<table width="100%" style="margin-top: 100px" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
    <tr>
        <td align="center" valign="top">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                <tr>
                    <td width="100%" class="mobile" align="center" valign="top">
                       <img src="https://office.hanara.id/img/hanara-logo.png" alt="" width="150px">
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="mobile" align="center" valign="top" >
                        <h1>
                            {{ $subject }}
                        </h1>
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="mobile" align="center" valign="top">
                        <p style="padding-left: 20px; padding-right: 20px; margin-top:10px; font-size:17px;">
                            Hi {{ $name }} <br> {!! $description !!}
                        </p>
                    </td>
                </tr>
                
                <tr>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="10" style="font-size:10px; line-height:20px; margin-top: 100px;">
            This email is a service provided by PT Hanara Sentra Teknologi.
            © 2021 PT Hanara Sentra Teknologi. All rights reserved.
            If you'd like to unsubscribe and stop receiving these emails 
            <a href="http://www.localhost:8000/email_blast/unsubscribe/{{ $id }}"> click here .</a>
        </td>
    </tr>
</table>
