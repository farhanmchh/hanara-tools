@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Email Blast</h1>
</div>

<a href="/email_blast/create" class="btn btn-primary mb-3">
	<i class="fas fa-plus"></i> Create Email Blast
</a>

@if (session('success'))
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<strong>{{ session('success') }}</strong>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
@endif


<div class="table-responsive">
	<table class="table table-striped table-sm">
		<thead>
			<tr>
				<th scope="col">Kategori</th>
				<th scope="col">Subject</th>
				<th scope="col">Delivered</th>
				<th scope="col">Blast_at</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			
			@foreach ($email_blast as $item)
			<tr>
				<form action="/email/sendEmail" class="d-inline" method="POST" enctype="multipart/form-data">
					@csrf

					<input type="hidden" name="id" value="{{ $item->id }}">
				<td>
					@if ($item->category == null)
					{{ $item->customerType->name ?? $item->category->name }}
					@else
					{{ $item->customerType->name ??  $item->category->name  }}
					@endif
				</td>
				<td>{{ $item->subject }}</td>
				<td>{{ $item->delivered == 1 ? 'Yes' : 'No';  }}</td>
				<td>{{ date('d M Y - H:i', strtotime($item->blast_at))}}</td>
				<td>
                </form>
					<a href="/email_blast/{{ $item->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
					<a href="/email_blast/{{ $item->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
					<form action="/email_blast/{{ $item->id }}" method="POST" class="d-inline">
						@method('delete')
						@csrf
						<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
					</form>
                    
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection