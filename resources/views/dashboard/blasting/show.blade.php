
@extends('dashboard.layouts.main')

@section('content')
<table width="100%" style="margin-top: 100px" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
    <tr>
        <td align="center" valign="top">

            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                <tr>
                    <td width="100%" class="mobile" align="center" valign="top">
                       <img src="{{ asset('img/hanara-logo.png') }}" alt="">
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="mobile" align="center" valign="top" >
                        <h1>
                            {{ $email_blast->subject }}
                        </h1>
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="mobile" align="center" valign="top">
                        <p class="ft-sz-16" style="padding-left: 20px; padding-right: 20px; margin-top:10px;">
                            {!! $email_blast->description !!}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="mobile" align="center" valign="top" style="margin-top:30px;">
                        <a href="/email_blast" class="btn btn-info btn-sm">
                        <i class="fas fa-angle-left"></i>
                        </a>
                        <a href="/email_blast/{{ $email_blast->id }}/edit" class="btn btn-warning btn-sm">
                            <i class="fas fa-edit"></i> Edit
                        </a>
                    </td>
                </tr>
                <tr>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
    </tr>
</table>
@endsection