@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Detail Minutes of Meeting</h1>
</div>

<a href="/mom" class="btn btn-info btn-sm">
  <i class="fas fa-angle-left"></i>
</a>

<h4 class="my-3">Summary</h4>

<div class="table-responsive">
  <table class="table table-sm table-bordered border-dark">
    <tr>
      <th colspan="6"><i>Meeting Subject :<br>{{ $mom->subject }}</i></th>
    </tr>
    <tr>
      <th><i>Date</i></th>
      <td>{{ $mom->date }}</td>
      <th><i>From</i></th>
      <td>{{ $mom->from }}</td>
      <th><i>To</i></th>
      <td>{{ $mom->to }}</td>
    </tr>
    <tr>
      <th><i>Location</i></th>
      <td>{{ $mom->location }}</td>
      <th><i>Note Taker</i></th>
      <td>{{ $mom->note_taker }}</td>
      <th><i>Duration</i></th>
      <td>{{ $mom->duration }}</td>
    </tr>
  </table>
</div>

<h4 class="my-3">Agenda</h4>

<div class="table-responsive text-center">
  <table class="table table-sm table-bordered border-dark">
    <tr>
      <th><i>No</i></th>
      <th><i>Topic</i></th>
    </tr>
    @foreach ($mom->momTopic as $momTopic)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $momTopic->topic }}</td>
      </tr>
    @endforeach
  </table>
</div>

<h4 class="my-3">Attendee List</h4>

<div class="table-responsive text-center">
  <table class="table table-sm table-bordered border-dark">
    <tr>
      <th><i>No</i></th>
      <th><i>Name</i></th>
      <th><i>Company</i></th>
      <th><i>Position</i></th>
      <th><i>Email</i></th>
    </tr>
    @foreach ($mom->momAttendee as $momAttendee)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $momAttendee->name }}</td>
        <td>{{ $momAttendee->company }}</td>
        <td>{{ $momAttendee->position }}</td>
        <td>{{ $momAttendee->email }}</td>
      </tr>
    @endforeach
  </table>
</div>

<h4 class="my-3">Issue and Action Plan</h4>

<div class="table-responsive">
  <table class="table table-sm table-bordered border-dark">
    <tr class="text-center">
      <th><i>No</i></th>
      <th><i>Issue Description</i></th>
      <th><i>Type</i></th>
      <th><i>Assigned To</i></th>
      <th><i>Due Date</i></th>
      <th><i>Resolution/Status</i></th>
      <th><i>Date Resolved</i></th>
    </tr>
    @foreach ($mom->momIssue as $momIssue)
      <tr>
        <td class="text-center">{{ $loop->iteration }}</td>
        <td>{!! $momIssue->description !!}</td>
        <td class="text-center">{{ $momIssue->type }}</td>
        <td class="text-center">{{ $momIssue->assigned }}</td>
        <td class="text-center">{{ $momIssue->due_date }}</td>
        <td class="text-center">{{ $momIssue->status }}</td>
        <td class="text-center">{{ $momIssue->date_resolved }}</td>
      </tr>
    @endforeach
  </table>
</div>

<div class="my-4">
  <h5>Terms & Conditions</h5>
  <p><b>{!! $mom->tnc !!}</b></p>
</div>

<div class="table-responsive">
  <table class="table table-sm table-bordered border-dark">
    <tr>
      <th width="10%"><i>Prepared By</i></th>
      <td width="50%">{{ $mom->note_taker }}</td>
      <td rowspan="2"></td>
    </tr>
    <tr>
      <th><i>Date</i></th>
      <td>{{ $mom->date }}</td>
    </tr>
  </table>
</div>

<div class="table-responsive">
  <table class="table table-sm table-bordered border-dark">
    <tr>
      <th width="10%"><i>Verified By</i></th>
      <td width="50%">{{ $mom->verified_by }}</td>
      <td rowspan="2"></td>
    </tr>
    <tr>
      <th><i>Date</i></th>
      <td>{{ $mom->verified_date }}</td>
    </tr>
  </table>
</div>

@endsection