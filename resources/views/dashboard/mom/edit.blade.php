@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Edit Minutes of Meeting</h1>
</div>

<form action="/mom/{{ $mom->id }}" method="POST">
  @method('PUT')
  @csrf
  {{-- Summary --}}
  <h4 class="my-3">Summary</h4>

  <div class="row">
    <div class="col-md">
      <div class="mb-3 row">
        <label for="subject" class="col-sm-auto form-label fw-bolder">Meeting Subject :</label>
        <div class="col-sm">
          <input type="text" name="subject" id="subject" class="form-control form-control-sm" value="{{ $mom->subject }}">
        </div>
      </div>
    </div>
  </div>

  <div class="row mb-3">
    <div class="col-md-6 border-end border-2">
      <div class="mb-3 row">
        <label for="date" class="col-sm-3 form-label fw-bolder">Date</label>
        <div class="col-sm-9">
          <input type="date" name="date" id="date" class="form-control form-control-sm text-center" value="{{ $mom->date }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label for="location" class="col-sm-3 form-label fw-bolder">Location</label>
        <div class="col-sm-9">
          <input type="text" name="location" id="location" class="form-control form-control-sm" value="{{ $mom->location }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label for="note_taker" class="col-sm-3 form-label fw-bolder">Note Taker</label>
        <div class="col-sm-9">
          <input type="text" name="note_taker" id="noteTaker" class="form-control form-control-sm" value="{{ $mom->note_taker }}">
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="mb-3 row">
        <label for="from" class="col-sm-3 form-label fw-bolder">From</label>
        <div class="col-sm-9">
          <input type="time" name="from" id="from" class="form-control form-control-sm" value="{{ $mom->from }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label for="to" class="col-sm-3 form-label fw-bolder">To</label>
        <div class="col-sm-9">
          <input type="time" name="to" id="to" class="form-control form-control-sm" value="{{ $mom->to }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label for="duration" class="col-sm-3 form-label fw-bolder">Duration</label>
        <div class="col-sm-9">
          <input type="text" name="duration" id="duration" class="form-control form-control-sm" value="{{ $mom->duration }}">
        </div>
      </div>
    </div>
  </div>
  {{-- End Summary --}}

  {{-- Agenda --}}
  <h4 class="my-3">Agenda</h4>
  
  <div class="table-responsive mb-0">
    <table class="table table-bordered table-sm text-center">
      <thead>
        <tr>
          <th width="5%">No</th>
          <th>Topic</th>
          <th width="7%">Action</th>
        </tr>
      </thead>
      <tbody id="agendaContainer">
        @foreach ($mom->momTopic as $momTopic)
          <tr class="agenda-row">
            <td>{{ $loop->iteration }}</td>
            <td>
              <input type="text" name="topic[]" class="form-control form-control-sm" value="{{ $momTopic->topic }}">
            </td>
            <td></td>
          </tr>
        @endforeach
      </tbody>
      <tbody>
        <tr>
          <td>
            <button type="button" id="addRowAgenda" class="btn btn-outline-info btn-sm">
              <i class="fas fa-plus"></i>
            </button> 
          </td>
          <td colspan="2"></td>
        </tr>
      </tbody>
    </table>
  </div>
  {{-- End Agenda --}}

  {{-- Attendee List --}}
  <h4 class="my-3">Attendee List</h4>

  <div class="table-responsive mb-3">
    <table class="table table-bordered table-sm text-center">
      <thead>
        <tr>
          <th width="5%">No</th>
          <th>Name</th>
          <th>Company</th>
          <th>Position</th>
          <th>Email</th>
          <th width="7%">Action</th>
        </tr>
      </thead>
      <tbody id="attendeeContainer">
        @foreach ($mom->momAttendee as $momAttendee)
          <tr class="attendee-row">
            <td>{{ $loop->iteration }}</td>
            <td>
              <input type="text" name="name[]" class="form-control form-control-sm" value="{{ $momAttendee->name }}">
            </td>
            <td>
              <input type="text" name="company[]" class="form-control form-control-sm" value="{{ $momAttendee->company }}">
            </td>
            <td>
              <input type="text" name="position[]" class="form-control form-control-sm" value="{{ $momAttendee->position }}">
            </td>
            <td>
              <input type="text" name="email[]" class="form-control form-control-sm" value="{{ $momAttendee->email }}">
            </td>
            <td></td>
          </tr>
        @endforeach
      </tbody>
      <tbody>
        <tr>
          <td>
            <button type="button" id="addRowAttendee" class="btn btn-outline-info btn-sm">
              <i class="fas fa-plus"></i>
            </button> 
          </td>
          <td colspan="5"></td>
        </tr>
      </tbody>
    </table>
  </div>
  {{-- End Attendee List --}}

  {{-- Issue --}}
  <h4 class="my-3">Issue and Action Plan</h4>

  <div class="table-responsive mb-3">
    <table class="table table-bordered table-sm">
      <thead class="text-center">
        <tr>
          <th>No</th>
          <th>Description</th>
          <th>Type</th>
          <th>Assigned To</th>
          <th>Due Date</th>
          <th>Resolution/Status</th>
          <th>Date Resolved</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody id="issueContainer" class="align-middle">
        @foreach ($mom->momIssue as $momIssue)
          <tr class="issue-row">
            <td class="text-center">{{ $loop->iteration }}</td>
            <td>
              <input id="description" type="hidden" name="description[]" value="{{ $momIssue->description }}">
              <trix-editor input="description" class="@error('description') form-control is-invalid @enderror" style="width: 300px"></trix-editor>
            </td>
            <td>
              <select name="type[]" id="type" class="form-select form-select-sm p-0" style="width: 70px">
                <option></option>
                <option value="info" @if($momIssue->type == 'info') selected @endif>Info</option>
                <option value="to do" @if($momIssue->type == 'to do') selected @endif>To Do</option>
                <option value="tba" @if($momIssue->type == 'tba') selected @endif>TBA</option>
                <option value="tbc" @if($momIssue->type == 'tbc') selected @endif>TBC</option>
                <option value="tbd" @if($momIssue->type == 'tbd') selected @endif>TBD</option>
              </select>
            </td>
            <td>
              <input type="text" name="assigned[]" class="form-control form-control-sm" value="{{ $momIssue->assigned }}">
            </td>
            <td>
              <input type="date" name="due_date[]" class="form-control form-control-sm" style="width: 135px" value="{{ $momIssue->due_date }}">
            </td>
            <td>
              <input type="text" name="status[]" class="form-control form-control-sm" value="{{ $momIssue->status }}">
            </td>
            <td>
              <input type="date" name="date_resolved[]" class="form-control form-control-sm" style="width: 135px" value="{{ $momIssue->date_resolved }}">
            </td>
            <td></td>
          </tr>
        @endforeach
      </tbody>
      <tbody>
        <tr>
          <td>
            <button type="button" id="addRowIssue" class="btn btn-outline-info btn-sm">
              <i class="fas fa-plus"></i>
            </button> 
          </td>
          <td colspan="7"></td>
        </tr>
      </tbody>
    </table>
  </div>
  {{-- End Issue --}}

  {{-- Terms & Conditions --}}
  <h5 class="my-3">Terms & Conditions</h5>

  <input id="tnc" type="hidden" name="tnc" value="{{ $mom->tnc }}">
  <trix-editor input="tnc" class="@error('tnc') form-control is-invalid @enderror mb-5" width="100px"></trix-editor>
  {{-- End Terms & Conditions --}}

  {{-- Footer --}}
  <div class="row">
    <div class="col-md-6 border-end border-2 mb-3">
      <div class="mb-3 row">
        <label class="col-sm-3 form-label fw-bolder">Prepared By</label>
        <div class="col-sm-9">
          <input type="text" name="" id="preparedBy" class="form-control form-control-sm" disabled>
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 form-label fw-bolder">Date</label>
        <div class="col-sm-9">
          <input type="text" name="" id="preparedDate" class="form-control form-control-sm" disabled>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="mb-3 row">
        <label class="col-sm-3 form-label fw-bolder">Verified By</label>
        <div class="col-sm-9">
          <input type="text" name="verified_by" id="verifieddBy" class="form-control form-control-sm" value="{{ $mom->verified_by }}">
        </div>
      </div>
      <div class="mb-3 row">
        <label class="col-sm-3 form-label fw-bolder">Date</label>
        <div class="col-sm-9">
          <input type="date" name="verified_date" id="verifiedDate" class="form-control form-control-sm text-center" value="{{ $mom->verified_date }}">
        </div>
      </div>
    </div>
  </div>
  {{-- End Footer --}}

  <div class="d-grid mb-5">
    <button type="submit" class="btn btn-warning">
      <i class="fas fa-paper-plane"></i> Update
    </button>
  </div>
</form>

<script>
  $(document).ready(function() {
    preparedBy()
    preparedDate()
  })
  
  $('#date').change(() => preparedDate())
  $('#noteTaker').keyup(() => preparedBy())

  function preparedDate() {
    return $('#preparedDate').val($('#date').val())
  }
  function preparedBy() {
    return $('#preparedBy').val($('#noteTaker').val())
  }
</script>

{{-- JS for Agenda --}}
<script>
  $('#addRowAgenda').click(() => $('#agendaContainer').append(agendaRow()))

  $(document).on('click', '.delete-agenda-row', function() {
    $(this).closest('.agenda-row').remove()

    $(".agenda-number").each(function(index){
      $(this).html(index + 2)
    })
  })

  function agendaRow() {
    return `<tr class="agenda-row">
              <td class="agenda-number">${$(".agenda-row").length + 1}</td>
              <td>
                <input type="text" name="topic[]" class="form-control form-control-sm">
              </td>
              <td>
                <a href="javascript:void(0)" class="delete-agenda-row btn btn-danger btn-sm">
                  <i class="fas fa-times-circle"></i>
                </a>
              </td>
            </tr>`
  }
</script>
{{-- End JS for Agenda --}}

{{-- JS for Attendee List --}}
<script>
  $('#addRowAttendee').click(() => $('#attendeeContainer').append(attendeeRow()))

  $(document).on('click', '.delete-attendee-row', function() {
    $(this).closest('.attendee-row').remove()

    $(".attendee-number").each(function(index){
      $(this).html(index + 2)
    })
  })

  function attendeeRow() {
    return `<tr class="attendee-row">
              <td class="attendee-number">${$(".attendee-row").length + 1}</td>
              <td>
                <input type="text" name="name[]" class="form-control form-control-sm">
              </td>
              <td>
                <input type="text" name="company[]" class="form-control form-control-sm">
              </td>
              <td>
                <input type="text" name="position[]" class="form-control form-control-sm">
              </td>
              <td>
                <input type="text" name="email[]" class="form-control form-control-sm">
              </td>
              <td>
                <a href="javascript:void(0)" class="delete-attendee-row btn btn-danger btn-sm">
                  <i class="fas fa-times-circle"></i>
                </a>
              </td>
            </tr>`
  }
</script>
{{-- End JS for Attendee List --}}

{{-- JS for Issue --}}
<script>
  $('#addRowIssue').click(() => $('#issueContainer').append(issueRow()))

  $(document).on('click', '.delete-issue-row', function() {
    $(this).closest('.issue-row').remove()

    $(".issue-number").each(function(index){
      $(this).html(index + 2)
    })
  })

  function issueRow() {
    return `<tr class="issue-row">
              <td class="issue-number text-center">${$('.issue-row').length + 1}</td>
              <td>
                <trix-editor input="newDescription" class="@error('description') form-control is-invalid @enderror" style="width: 300px"></trix-editor>
                <input id="newDescription" type="hidden" name="description[]">
              </td>
              <td>
                <select name="type[]" id="type" class="form-select form-select-sm p-0" style="width: 70px">
                  <option></option>
                  <option value="info">Info</option>
                  <option value="to do">To Do</option>
                  <option value="tba">TBA</option>
                  <option value="tbc">TBC</option>
                  <option value="tbd">TBD</option>
                </select>
              </td>
              <td>
                <input type="text" name="assigned[]" class="form-control form-control-sm">
              </td>
              <td>
                <input type="date" name="due_date[]" class="form-control form-control-sm" style="width: 135px">
              </td>
              <td>
                <input type="text" name="status[]" class="form-control form-control-sm">
              </td>
              <td>
                <input type="date" name="date_resolved[]" class="form-control form-control-sm" style="width: 135px">
              </td>
              <td>
                <a href="javascript:void(0)" class="delete-issue-row btn btn-danger btn-sm">
                  <i class="fas fa-times-circle"></i>
                </a>
              </td>
            </tr>`
  }
</script>
{{-- End JS for Issue --}}

@endsection