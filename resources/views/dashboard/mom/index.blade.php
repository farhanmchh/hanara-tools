@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Minutes of Meeting</h1>
</div>

<a href="/mom/create" class="btn btn-primary mb-3">
  <i class="fas fa-plus"></i> Create new MoM
</a>

@if (session('success'))
  <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

<div class="table-responsive">

	<table class="table table-striped table-sm text-center">
		<thead>
			<tr>
				<th scope="col">No</th>
				<th scope="col">Subject</th>
				<th scope="col">Location</th>
				<th scope="col">Note Taker</th>
				<th scope="col">Date</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($moms as $mom)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $mom->subject }}</td>
				<td>{{ $mom->location }}</td>
				<td>{{ $mom->note_taker }}</td>
        <td>{{ $mom->date }}</td>
				<td>
					<a href="/mom/{{ $mom->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
					<a href="/print_mom/{{ $mom->id }}" class="btn btn-success btn-sm"><i class="fas fa-print"></i></a>
					<a href="/mom/{{ $mom->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
					<a href="/mom/email/{{ $mom->id }}" class="btn btn-primary btn-sm"><i class="fas fa-paper-plane"></i></a>
					<form action="/mom/{{ $mom->id }}" method="POST" class="d-inline">
						@method('delete')
						@csrf
						<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">
							<i class="fas fa-times-circle"></i>
						</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection