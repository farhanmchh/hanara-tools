@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Create New Domain</h1>
</div>

<a href="/domain" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<form action="/domain" method="post">
@csrf
<div class="row">
  <div class="col-md-4">
    <div class="mb-3">
      <label for="name" class="form-label fw-bolder">* Domain Name</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" autofocus value="{{ old('name') }}">
      @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="customer_id" class="form-label fw-bolder">* User</label>
      <select name="customer_id" id="customer_id" class="form-select @error('customer_id') is-invalid @enderror">
        <option></option>
        @foreach ($customers as $customer)
          @if (old('customer_id') == $customer->id)
          <option value="{{ $customer->id }}" selected>{{ $customer->name }}</option>
          @else
          <option value="{{ $customer->id }}">{{ $customer->name }}</option>
          @endif
        @endforeach
      </select>
      @error('customer_id')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label class="form-check-label fw-bolder">Hosting :</label>
      <div class="form-check form-switch ms-3">
        <input class="form-check-input" type="checkbox" name="hosting" id="hosting" @if(old('hosting') == 1) checked @endif>
      </div>
    </div>
    <div class="mb-3">
      <label class="form-check-label fw-bolder">Control Panel :</label>
      <div class="form-check form-switch ms-3">
        <input class="form-check-input" type="checkbox" name="cpanel" id="cpanel" @if(old('cpanel') == 1) checked @else disabled @endif>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="mb-3">
      <label for="user-cpanel" class="form-label fw-bolder">User Control Panel</label>
      <input type="text" name="user_cpanel" class="form-control @error('user_cpanel') is-invalid @enderror" id="user-cpanel" disabled
      value="{{ old('user_cpanel') }}">
      @error('user_cpanel')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="password-cpanel" class="form-label fw-bolder">Password Control Panel</label>
      <input type="text" name="password_cpanel" class="form-control @error('password_cpanel') is-invalid @enderror" id="password-cpanel" disabled
      value="{{ old('password_cpanel') }}">
      @error('password_cpanel')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="vendor_id" class="form-label fw-bolder">* Source</label>
      <select name="vendor_id" id="vendor_id" class="form-select @error('vendor_id') is-invalid @enderror">
        <option></option>
        @foreach ($vendors as $vendor)
          @if (old('vendor_id') == $vendor->id)
          <option value="{{ $vendor->id }}" selected>{{ $vendor->name }}</option>
          @else
          <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
          @endif
        @endforeach
      </select>
      @error('vendor_id')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="expired" class="form-label fw-bolder">* Expired</label>
      <input type="date" name="expired" class="form-control @error('expired') is-invalid @enderror" id="expired" value="{{ old('expired') }}">
      @error('expired')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
  </div>
  <div class="col-md-4">
    <div class="mb-3" id="nameServerContainer">
      <div class="row align-items-end justify-content-between mb-2">
        <div class="col-sm-auto">
          <label for="name_server" class="form-label fw-bolder">Name Server</label>
        </div>
        <div class="col-sm-auto">
          <button type="button" id="addNameServer" class="btn btn-outline-info btn-sm">
            <i class="fas fa-plus"></i>
          </button>
        </div>
      </div>
      <div class="input-container input-group mb-1">

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md d-grid">
    <button type="submit" class="btn btn-primary">
      <i class="fas fa-paper-plane"></i> Create
    </button>
  </div>
</div>
</form>

{{-- Switch Validation --}}
<script>
  const hosting = document.querySelector('#hosting');
  const cpanel = document.querySelector('#cpanel');
  const userCpanel = document.querySelector('#user-cpanel');
  const passwordCpanel = document.querySelector('#password-cpanel');
  
  hosting.addEventListener('click', function() {
    if (hosting.checked) {
      cpanel.removeAttribute('disabled');
      if (cpanel.checked) {
        userCpanel.removeAttribute('disabled');
        passwordCpanel.removeAttribute('disabled');
      }
      hosting.value = 1;
    } else {
      cpanel.setAttribute('disabled', '');
      userCpanel.setAttribute('disabled', '');
      passwordCpanel.setAttribute('disabled', '');
      userCpanel.value = '';
      passwordCpanel.value = '';
      hosting.value = 0;
    }
  });

  cpanel.addEventListener('click', function() {
    if (cpanel.checked) {
      userCpanel.removeAttribute('disabled');
      passwordCpanel.removeAttribute('disabled');
      cpanel.value = 1;
    } else {
      userCpanel.setAttribute('disabled', '');
      passwordCpanel.setAttribute('disabled', '');
      userCpanel.value = '';
      passwordCpanel.value = '';
      cpanel.value = 0;
    }
  });

  if (cpanel.checked) {
      userCpanel.removeAttribute('disabled');
      passwordCpanel.removeAttribute('disabled');
      cpanel.value = 1;
    }
</script>
{{-- End Switch Validation --}}

{{-- Name Server --}}
<script>
  $('#addNameServer').on('click', function() {
    let i;
    $(".input-container").each(function(index){
      i = index + 1
    })
      console.log(i)
    if (i != 5) {
      $('#nameServerContainer').append(nameServer())
    } else {
      return false
    }
  })

  $(document).on('dblclick', '.delete-row', function() {
    $(this).closest('.input-container').remove()
  })

  function nameServer() {
    return `<div class="input-container input-group mb-1">
              <input type="text" name="name_server[]" class="form-control">
              <a href="javascript:void(0)" class="delete-row btn btn-outline-danger">
                <i class="fas fa-times-circle"></i>
              </a>
            </div>`
  }
</script>
{{-- End Name Server --}}

@endsection