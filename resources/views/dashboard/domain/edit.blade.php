@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Edit Domain</h1>
</div>

<a href="/domain" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<form action="/domain/{{ $domain->id }}" method="POST">
  @method('PUT')
  @csrf
  <div class="row ">
    <div class="col-md-4">
      <div class="mb-3">
        <label class="form-label fw-bolder">* Domain Name</label>
        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $domain->name }}">
        @error('name')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="mb-3">
        <label class="form-label fw-bolder">* User</label>
        <select name="customer_id" class="form-select">
          @foreach ($customers as $customer)
            @if ($domain->customer_id == $customer->id)
              <option value="{{ $customer->id }}" selected>{{ $customer->name }}</option>
            @else
              <option value="{{ $customer->id }}">{{ $customer->name }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="mb-3">
        <label class="form-check-label fw-bolder">Hosting :</label>
        <div class="form-check form-switch ms-3">
          <input class="form-check-input" type="checkbox" name="hosting" id="hosting" @if($domain->hosting == 1) checked @endif>
        </div>
      </div>
      <div class="mb-3">
        <label class="form-check-label fw-bolder">Control Panel :</label>
        <div class="form-check form-switch ms-3">
          <input class="form-check-input" type="checkbox" name="cpanel" id="cpanel" @if($domain->cpanel == 1) checked @endif>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="mb-3">
        <label for="province" class="form-label fw-bolder">User Control Panel</label>
        <input type="text" name="user_cpanel" class="form-control @error('user_cpanel') is-invalid @enderror" id="user-cpanel"
        value="{{ $domain->user_cpanel }}">
      </div>
      <div class="mb-3">
        <label for="city" class="form-label fw-bolder">Password Control Panel</label>
        <input type="text" name="password_cpanel" class="form-control @error('password_cpanel') is-invalid @enderror" id="password-cpanel" 
        value="{{ $domain->password_cpanel }}">
      </div>
      <div class="mb-3">
        <label class="form-label fw-bolder">Source</label>
        <select name="vendor_id" class="form-select">
          @foreach ($vendors as $vendor)
            @if ($domain->vendor_id == $vendor->id)
              <option value="{{ $vendor->id }}" selected>{{ $vendor->name }}</option>
            @else
              <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="mb-3">
        <label class="form-label fw-bolder">Expired</label>
        <input type="date" name="expired" class="form-control @error('expired') is-invalid @enderror" value="{{ $domain->expired }}">
      </div>

    </div>

    <div class="col-md-4">
      <div class="mb-3">
        <div class="mb-3" id="nameServerContainer">
          <div class="row align-items-end justify-content-between mb-2">
            <div class="col-sm-auto">
              <label for="name_server" class="form-label fw-bolder">Name Server</label>
            </div>
            <div class="col-sm-auto">
              <button type="button" id="addNameServer" class="btn btn-outline-info btn-sm">
                <i class="fas fa-plus"></i>
              </button>
            </div>
          </div>
          @foreach ($domain->nameServer as $nameServer)
            <div class="input-container input-group mb-1">
              <input type="text" name="name_server[]" class="form-control" value="{{ $nameServer->name }}">
              <a href="javascript:void(0)" class="delete-row btn btn-outline-danger">
                <i class="fas fa-times-circle"></i>
              </a>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md d-grid">
      <button type="submit" class="btn btn-warning">
        <i class="fas fa-paper-plane"></i> Update
      </button>
    </div>
  </div>
</form>

{{-- Switch Validation --}}
<script>
  const hosting = document.querySelector('#hosting');
  const cpanel = document.querySelector('#cpanel');
  const userCpanel = document.querySelector('#user-cpanel');
  const passwordCpanel = document.querySelector('#password-cpanel');

  if (!hosting.checked) {
    cpanel.setAttribute('disabled', '');
    hosting.value = 0;
    console.log(hosting.value)
  }

  if (!cpanel.checked) {
    userCpanel.setAttribute('disabled', '');
    passwordCpanel.setAttribute('disabled', '');
    cpanel.value = 0;
    console.log(cpanel.value)
  }

  hosting.addEventListener('click', function() {
    if (hosting.checked) {
      cpanel.removeAttribute('disabled');
      if (cpanel.checked) {
        userCpanel.removeAttribute('disabled');
        passwordCpanel.removeAttribute('disabled');
      }
      hosting.value = 1;
    } else {
      cpanel.setAttribute('disabled', '');
      userCpanel.setAttribute('disabled', '');
      passwordCpanel.setAttribute('disabled', '');
      userCpanel.value = '';
      passwordCpanel.value = '';
      hosting.value = 0;
    }
  });

  cpanel.addEventListener('click', function() {
    if (cpanel.checked) {
      userCpanel.removeAttribute('disabled');
      passwordCpanel.removeAttribute('disabled');
      cpanel.value = 1;
    } else {
      userCpanel.setAttribute('disabled', '');
      passwordCpanel.setAttribute('disabled', '');
      userCpanel.value = '';
      passwordCpanel.value = '';
      cpanel.value = 0;
    }
  });
</script>
{{-- End Swith Validation --}}

<script>
  $('#addNameServer').on('click', function() {
    let i;
    $(".input-container").each(function(index){
      i = index + 2
    })
    console.log(i)
    if (i != 5) {
      $('#nameServerContainer').append(nameServer())
    } else {
      return false
    }
  })

  $(document).on('dblclick', '.delete-row', function() {
    $(this).closest('.input-container').remove()
  })

  function nameServer() {
    return `<div class="input-container input-group mb-1">
              <input type="text" name="name_server[]" class="form-control">
              <a href="javascript:void(0)" class="delete-row btn btn-outline-danger">
                <i class="fas fa-times-circle"></i>
              </a>
            </div>`
  }
</script>

@endsection