@extends('dashboard.layouts.main')

@section('content')
  
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Detail Domain</h1>
</div>

<div class="mb-4">
  <a href="/domain" class="btn btn-outline-secondary btn-sm">
    <i class="fas fa-arrow-left"></i>
  </a>
  <a href="/domain/{{ $domain->id }}/edit" class="btn btn-warning btn-sm">
    <i class="fas fa-edit"></i> Edit
  </a>
  <form action="/domain/{{ $domain->id }}" method="POST" class="d-inline">
    @method('delete')
    @csrf
    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this domain?')">
      <i class="fas fa-trash"></i> Delete
    </button>
  </form>
</div>

@if (session('success'))
  <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
  
<div class="row ">
  <div class="col-md-4">
    <div class="mb-3">
      <label class="form-label fw-bolder">Domain Name</label>
      <p class="border-bottom border-dark fs-6 ps-2 pb-2">{{ $domain->name }}</p>
    </div>
    <div class="mb-3">
      <label class="form-label fw-bolder">User</label>
      <p class="border-bottom border-dark fs-6 ps-2 pb-2">{{ $domain->customer->name }}</p>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-4 form-check-label fw-bolder">Hosting :</label>
      <div class="col-sm-8">
        @if ($domain->hosting)
          <i class="far fa-check-circle text-success fs-6"></i>
        @else
        <i class="far fa-times-circle text-danger fs-6"></i>
        @endif
      </div>
    </div>
    <div class="mb-3 row">
      <label class="col-sm-4 form-check-label fw-bolder">Control Panel :</label>
      <div class="col-sm-8">
        @if ($domain->cpanel)
          <i class="far fa-check-circle text-success fs-6"></i>
          @else
          <i class="far fa-times-circle text-danger fs-6"></i>
          @endif
        </div>
      </div>
    </div>
    
    <div class="col-md-4">
      <div class="mb-3">
        <label for="province" class="form-label fw-bolder">User Control Panel</label>
        <p class="border-bottom border-dark fs-6 ps-2 pb-2">{{ $domain->user_cpanel ?? '-' }}</p>
      </div>
      <div class="mb-3">
        <label for="city" class="form-label fw-bolder">Password Control Panel</label>
        <p class="border-bottom border-dark fs-6 ps-2 pb-2">{{ $domain->password_cpanel ?? '-' }}</p>
      </div>
    <div class="mb-3">
      <label class="form-label fw-bolder">Source</label>
      <p class="border-bottom border-dark fs-6 ps-2 pb-2">{{ $domain->vendor->name }}</p>
    </div>
    <div class="mb-3">
      <label class="form-label fw-bolder">Expired</label>
      <p class="border-bottom border-dark fs-6 ps-2 pb-2">{{ $domain->expired }}</p>
    </div>
    
  </div>
  
  <div class="col-md-4">
    <div class="mb-3">
      <label for="name_server" class="form-label fw-bolder">Name Server</label>
      @foreach ($domain->nameServer as $nameServer)
        <div class="mb-1">
          <p class="border-bottom border-dark fs-6 ps-2 pb-2">{{ $nameServer->name }}</p>
        </div>
      @endforeach
    </div>
  </div>
</div>

@endsection