@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Domain</h1>
</div>

<a href="/domain/create" class="btn btn-primary mb-3">
  <i class="fas fa-plus"></i> Create a New Domain
</a>

@if (session('success'))
  <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

@if (session('error'))
  <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('error') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

<div class="table-responsive">
  <table class="table table-striped table-sm text-center">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Domain Name</th>
        <th scope="col">User</th>
        <th scope="col">Source</th>
        <th scope="col">Expired</th>
        <th scope="col">Hosting</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($domains as $domain)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $domain->name }}</td>
        <td>{{ $domain->customer->name }}</td>
        <td>{{ $domain->vendor->name }}</td>
        <td>{{ $domain->expired }}</td>
        <td>
          @if ($domain->hosting)
            <i class="far fa-check-circle text-success fs-6"></i>
          @else
            <i class="far fa-times-circle text-danger fs-6"></i>
          @endif
        </td>
        <td>
          <a href="/domain/{{ $domain->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
          <a href="/domain/{{ $domain->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
          <form action="/domain/{{ $domain->id }}" method="POST" class="d-inline">
            @method("delete")
            @csrf
            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure to delete this domain?')"><i class="fas fa-trash"></i></button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection