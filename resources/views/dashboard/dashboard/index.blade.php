@extends('dashboard.layouts.main')
@section('content')
    
<div class="row row-cols-1 row-cols-md-3 g-4 mt-3 justify-content-center text-center">
  <div class="col">
    <div class="card border-3 shadow" style="background-color: rgb(251, 235, 217);">
      <div class="card-body">
        <h5 class="card-title fs-1   ">{{ $customers }}</h5>
        <hr>
        <p class="card-text"><i class="fas fa-users"></i> Customers</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-3 shadow" style="background-color:rgb(251, 249, 217);">
      <div class="card-body">
        <h5 class="card-title fs-1">{{ $domains }}</h5>
        <hr>
        <p class="card-text"><i class="fas fa-globe"></i> Domains</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-3 shadow" style="background-color:rgb(232, 251, 217);">
      <div class="card-body">
        <h5 class="card-title fs-1">{{ $vendors }}</h5>
        <hr>
        <p class="card-text"><i class="fas fa-handshake"></i> Vendors</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-3 shadow" style="background-color:rgb(217, 251, 242);">
      <div class="card-body">
        <h5 class="card-title fs-1">{{ $staff }}</h5>
        <hr>
        <p class="card-text"><i class="fas fa-address-book"></i> Staffs</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-3 shadow" style="background-color:rgb(217, 227, 251);">
      <div class="card-body">
        <h5 class="card-title fs-1">{{ $project }}</h5>
        <hr>
        <p class="card-text"><i class="fas fa-tasks"></i> Projects</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-3 shadow" style="background-color:rgb(245, 217, 251);">
      <div class="card-body">
        <h5 class="card-title fs-1">{{ $quotation }}</h5>
        <hr>
        <p class="card-text"><i class="fas fa-envelope-open-text"></i> Quotation</p>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-3 shadow" style="background-color:rgb(251, 217, 232);">
      <div class="card-body">
        <h5 class="card-title fs-1">{{ $quotation }}</h5>
        <hr>
        <p class="card-text"><i class="fab fa-stack-overflow"></i> Task</p>
      </div>
    </div>
  </div>
</div>

<div class="card mb-10 mt-4">
  <div class="card-body">
    <div class="row">
      <div class="col-md-10">
        <h5 class="card-title">Invoice</h5>
      </div>
      <div class="col-md-2">
        <p class="card-text"><small class="text-muted fs-6">5000 Data</small></p>
      </div>
    </div>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">First</th>
          <th scope="col">Last</th>
          <th scope="col">Handle</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td colspan="2">Larry the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<div class="row row-cols-1 mb-5 mt-4">
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title fs-1">Bar Chart</h5>
        <p class="card-text"><i class="fas fa-envelope-open-text"></i> Quotation Status</p>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title fs-1">Pie Chart</h5>
        <p class="card-text"><i class="fas fa-envelope-open-text"></i> Customer</p>
      </div>
    </div>
  </div>
</div>

  

@endsection