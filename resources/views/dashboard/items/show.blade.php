@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Detail Item</h2>
</div>

<div class="mb-3">
  <a href="/item" class="btn btn-info btn-sm">
    <i class="fas fa-angle-left"></i>
  </a>
  <a href="/item/{{ $item->id }}/edit" class="btn btn-warning btn-sm">
    <i class="fas fa-edit"></i> Edit
  </a>
  <form action="/item/{{ $item->id }}" method="POST" class="d-inline">
    @method('delete')
    @csrf
    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">
      <i class="fas fa-trash"></i> Delete
    </button>
  </form>
</div>

@if (session('success'))
  <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

<div class="col-md-6">
    <div class="mb-3">
      <label for="name" class="form-label fw-bolder">Name :</label>
      <p class="fs-6 border-bottom border-2 p-2">{{ $item->name }}</p>
    </div>
    <div class="mb-3">
      <label for="price" class="form-label fw-bolder">Price :</label>
      <p class="fs-6 border-bottom border-2 p-2">{{ $item->price }}</p>
    </div>
    <div class="mb-3">
      <label for="description" class="form-label fw-bolder">Description :</label>
      <p class="fs-6">{!! $item->description !!}</p>
    </div>
</div>

@endsection