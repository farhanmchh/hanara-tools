@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Edit Items</h2>
</div>

<a href="/item" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel 
</a>

<form action="/item/{{ $item->id }}" method="post">
@method('PUT')
@csrf
  <div class="col-md-6">
    <div class="mb-3">
      <label for="name" class="form-label">Name</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"
      value="{{ (old('name') ? old('name') : $item->name) }}">
      @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="price" class="form-label">Price</label>
      <input type="text" name="price" class="form-control @error('price') is-invalid @enderror" id="price" 
      value="{{ (old('price') ? old('price') : $item->price) }}">
      @error('price')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>  
    <div class="mb-3">
      <label class="form-label">Description</label>
      <input id="description" type="hidden" name="description" value="{{ (old('description') ? old('description') : $item->description) }}">
      <trix-editor input="description" class="@error('description') form-control is-invalid @enderror"></trix-editor>
      @error('description')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror  
    </div>
  </div>

  <div class="col-md-6 d-grid">
    <button type="submit" class="btn btn-warning">
      <i class="fas fa-paper-plane"></i> Update
    </button>
  </div>
</form>
  
@endsection