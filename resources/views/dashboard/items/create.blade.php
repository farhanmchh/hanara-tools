@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h2>Create a New Item</h2>
</div>

<a href="/item" class="btn btn-danger btn-sm mb-3">
  <i class="fas fa-times-circle"></i> Cancel
</a>

<div class="col-md-6">
  <form action="/item" method="post">
    @csrf
    <div class="mb-3">
      <label for="name" class="form-label">Name</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}" autofocus autocomplete="off">
      @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="price" class="form-label">Price</label>
      <input type="text" name="price" class="form-control @error('price') is-invalid @enderror" id="price" value="{{ old('price') }}" autocomplete="off">
      @error('price')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
    </div>  
    <div class="mb-3">
      <label class="form-label">Description</label>
      <trix-editor input="x" class="@error('description') form-control is-invalid @enderror"></trix-editor>
      <input id="x" type="hidden" name="description">
      @error('description')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
      @enderror
      
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

{{-- <script>
  $('#price').click(function() {
    if (!$(this).val()) {
      $(this).val(`Rp. `)
    }

    $('#price').keyup(function () {
      if ($(this).val().length == 3) {
        $(this).val(`Rp. `)
      }

      let stringPrice = $(this).val()
      let arrPrice = stringPrice.split('')
      let price = ''
      
      if (arrPrice.length == 8) {
        $(this).val(thousand(arrPrice)) 
      }
      
      if (arrPrice.length == 10 || arrPrice.length == 9 && arrPrice[6] == '.') {
        $(this).val(tensOfThousands(arrPrice))
      }

      if (arrPrice.length == 11 || arrPrice.length == 10 && arrPrice[7] == '.') {
        
        $(this).val(hundredsOfThousand(arrPrice))
      }
      
      if (arrPrice.length == 12) {
        $(this).val(milions(arrPrice))
      }

      if (arrPrice.length == 13) {

      }
      
      // if (arrPrice.length == 14) {
      //   if (arrPrice[5] == '.') {
      //     arrPrice.splice(5, 1)
      //     arrPrice.splice(8, 1)
      //     arrPrice.splice(6, 0, '.')
      //     arrPrice.splice(10, 0, '.')
      //   }
      //   price += arrPrice.join('')
      //   $(this).val(price)
      // }

      console.log(arrPrice)
    })
  })

  function thousand(arr) {
    let price = ''
    if (arr[5] == '.') {
      arr.splice(5, 1)
    } else {
      arr.splice(5, 0, '.')
    }
    price += arr.join('')
    return price
  }

  function tensOfThousands(arr) {
    let price = ''
    if (arr[6] == '.') {
      arr.splice(6, 1)
      arr.splice(5, 0, '.')
    } else if (arr[7] == '.') {
      arr.splice(7, 1)
      arr.splice(6, 0, '.')
    } else {
      arr.splice(5, 1)
      arr.splice(6, 0, '.')
    }
    price += arr.join('')
    return price
  }

  function hundredsOfThousand(arr) {
    let price = ''
    if (arr[7] == '.') {
      arr.splice(7, 1)
      arr.splice(6, 0, '.')
    }else {
      arr.splice(6, 1)
      arr.splice(7, 0, '.')
    }
    price += arr.join('')
    return price
  }

  function milions(arr) {
    let price = ''
    arr.splice(7, 1)
    arr.splice(5, 0, '.')
    arr.splice(9, 0, '.')
    price += arr.join('')
    return price
  }
</script> --}}

@endsection