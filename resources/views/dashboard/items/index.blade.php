@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2 text-center">Items</h1>
</div>

<div class="table-responsive">
	<a href="/item/create" class="btn btn-primary mb-3">
		<i class="fas fa-plus"></i> Add new item
	</a>

	@if (session('success'))
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<strong>{{ session('success') }}</strong>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
	@endif

	@if ($check > 0)
	<table class="table table-striped table-sm text-center">
		<thead>
			<tr>
				<th scope="col">No</th>
				<th scope="col">Name</th>
				<th scope="col">Price</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($items as $item)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $item->name }}</td>
				<td>{{ formatPrice($item->price) }},-</td>
				<td>
					<a href="/item/{{ $item->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
					<a href="/item/{{ $item->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
					<form action="/item/{{ $item->id }}" method="POST" class="d-inline">
						@method("DELETE")
						@csrf
						<button class="btn btn-danger btn-sm" onclick="return confirm('Are You Sure?')"><i class="fas fa-trash"></i></button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<h1 class="text-center text-danger">Data empty</h1>
	@endif
</div>


@endsection