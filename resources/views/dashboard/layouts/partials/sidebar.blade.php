<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse" id="sidebar" style="overflow-y: auto; overflow-x: hidden;">
  <div class="position-sticky pt-3">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('dashboard*') ? 'active' : '' }}" href="/dashboard">
          <i class="col-sm-1 fas fa-chart-line"></i><p class="col-sm-auto d-inline"> Dashboard</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('quotation*') ? 'active' : '' }}" href="/quotation">
          <i class="col-sm-1 fas fa-envelope-open-text"></i><p class="col-sm-auto d-inline"> Quotation</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('item*') ? 'active' : '' }}" href="/item">
          <i class="col-sm-1 fas fa-list-alt"></i><p class="col-sm-auto d-inline"> Items</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('category*') ? 'active' : '' }}" href="/category">
          <i class="col-sm-1 fas fa-filter"></i><p class="col-sm-auto d-inline"> Categories</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('customer*') ? 'active' : '' }}" href="/customer">
          <i class="col-sm-1 fas fa-users"></i><p class="col-sm-auto d-inline"> Customers</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('vendor*') ? 'active' : '' }}" href="/vendor">
          <i class="col-sm-1 fas fa-handshake"></i><p class="col-sm-auto d-inline"> Vendors</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('domain*') ? 'active' : '' }}" href="/domain">
          <i class="col-sm-1 fas fa-globe"></i><p class="col-sm-auto d-inline"> Domains</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('invoice*') ? 'active' : '' }}" href="/invoice">
            <i class="col-sm-1 fas fa-file-invoice"></i><p class="col-sm-auto d-inline"> Invoice</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('event*') ? 'active' : '' }}" href="/event">
            <i class="col-sm-1 fas fa-calendar-check"></i><p class="col-sm-auto d-inline"> Events</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('task*') ? 'active' : '' }}" href="/task">
            <i class="col-sm-1 fas fa-tasks"></i><p class="col-sm-auto d-inline"> Tasks</p>
        </a>
      </li>
      <li class="sidebar-dropdown {{ Request::is('project*') ? 'active' : '' }}">
            <a class="nav-link dropdown-toggle" >
              <i class="col-sm-1 fa fa-code-branch"></i><p class="col-sm-auto d-inline"> Projects</p>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="dropdown-item" >
                <a class="nav-link {{ Request::is('project*') ? 'active' : '' }}" href="/project">
                  <i class="col-sm-1 fas fa-list"></i><p class="col-sm-auto d-inline"> List Projects</p> 
                </a>
              </li>
              <li class="dropdown-item" href="/project_category">
                <a class="nav-link {{ Request::is('project_category*') ? 'active' : '' }}" href="/project_category"> 
                  <i class="col-sm-1 fas fa-code-branch"></i><p class="col-sm-auto d-inline"> Category Projects</p> 
                </a>
              </li>
            </ul>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('employee*') ? 'active' : '' }}" href="/employee">
            <i class="col-sm-1 fas fa-user-tie"></i><p class="col-sm-auto d-inline"> Employees</p>
        </a>
     </li>
      <li class="nav-item">
        
        <a class="row nav-link {{ Request::is('mom*') ? 'active' : '' }}" href="/mom">
            <i class="col-sm-1 fab fa-medium-m"></i><p class="col-sm-auto d-inline"> Minutes of Meeting</p>
        </a>
     </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('email_blast*') ? 'active' : '' }}" href="/email_blast">
          <i class="col-sm-1 fas fa-mail-bulk"></i><p class="col-sm-auto d-inline"> Email Blast</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('email_scheduler*') ? 'active' : '' }}" href="/email_scheduler">
          <i class="col-sm-1 fas fa-calendar-alt"></i><p class="col-sm-auto d-inline"> Email scheduler</p>
        </a>
      </li>
      <div class="dropdown-divider"></div>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('profile*') ? 'active' : '' }}" href="/profile">
          <i class="col-sm-1 fas fa-user"></i><p class="col-sm-auto d-inline"> Profile</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="row nav-link {{ Request::is('tnc*') ? 'active' : '' }}" href="/tnc">
          <i class="col-sm-1 fas fa-file-signature"></i><p class="col-sm-auto d-inline"> Terms & Conditions</p>
        </a>
      </li>
    </ul>
  </div>
</nav>