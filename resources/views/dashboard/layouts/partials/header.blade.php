<header class="navbar navbar-dark sticky-top bg-warning flex-md-nowrap p-0">
  <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3 fw-bolder" href="/profile">
    <i class="fas fa-hospital-symbol h5"></i> Hanara ERP
  </a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="container justify-content-end">
    <div class="dropstart mx-4">
      <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
        <img src="/img/hanara-logo.png" alt="mdo" width="32" height="32" class="rounded-circle bg-white">
      </a>
      <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
        <li>
          <a class="dropdown-item" href="/profile">
            <i class="fas fa-user"></i> Profile
          </a>
        </li>
        <li>
          <a class="dropdown-item" href="/quotation/create">
            <i class="fas fa-plus"></i> Create New Quotation
          </a>
        </li>
        <li><hr class="dropdown-divider"></li>
        <li>
          <a class="dropdown-item" href="/logout">
            <i class="fas fa-sign-out-alt"></i> Logout
          </a>
        </li>
      </ul>
    </div>
  </div>
</header>