<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ $title }}</title>

	<!-- Bootstrap core CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	{{-- Font Awesome Icon --}}
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

	{{-- Select2 --}}
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.1.1/dist/select2-bootstrap-5-theme.min.css" />

	{{-- Trix Editor --}}
	<link rel="stylesheet" type="text/css" href="/css/trix.css">
  	<script type="text/javascript" src="/js/trix.js"></script>

	<!-- Custom styles for this template -->
	<link href="/css/dashboard.css" rel="stylesheet">

	<script src="{{ asset('libraries/combined/js/gijgo.min.js') }}"></script>
	{{-- Date Picker --}}
	<link rel="stylesheet" href="{{ asset('libraries/combined/css/gijgo.min.css') }}">
	{{-- Text Editor --}} 
	<script src="{{ asset('libraries/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset('libraries/ckfinder/ckfinder.js') }}"></script>

	<!-- include summernote css/js -->
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

	<link rel="stylesheet" href="/css/style.css">

	@hasSection('style')
		@yield('style')
	@endif
	<style>
		trix-toolbar [data-trix-button-group='file-tools'] {
			display: none;
		}
		.sidebar-dropdown.active .dropdown-menu{
			display: block;
			position: relative;    
			background-color: #dbd6d6 !important;
			border-radius: 0px !important;
		}
		.sidebar-dropdown.active .dropdown-item:focus, .sidebar-dropdown.active .dropdown-item:hover {
			color: #4487e1;
			background-color: #dbd6d6;
		}
		.dropdown-item::before{
			font-family: "Font Awesome 5 Pro";
			color : #6f28df;
			margin-left: .5vw;
			margin-right: .7vw;
		}
		.dropdown-toggle::after {
			float: right;
		}
		.dropdown-item {
			padding : none;
			font-size: 12px;
			display: block;
			width: 100%;
			clear: both;
			font-weight: 400;
			color: #212529;
			text-align: inherit;
			text-decoration: none;
			white-space: nowrap;
			background-color: transparent;
			border: 0;
		}
	</style>

</head>
<body>

	@include('dashboard.layouts.partials.header')

	<div class="container-fluid">
		<div class="row">

			@include('dashboard.layouts.partials.sidebar')

			<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
				@yield('content')
			</main>
			
		</div>
	</div>


	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
	
	<script src="/js/dashboard.js"></script>
	@hasSection('script')
		@yield('script')
	@endif
	<script>
		$( ".sidebar-dropdown").click(function() {
			if($(this).hasClass("active")){
				$(this).removeClass('active');
			}else{
				$(".sidebar-dropdown").removeClass('active');
				$(this).addClass('active');
			}
		});
	</script>
</body>
</html>
