@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Detail employee</h1>
    </div>

    <div class="mb-3">
        <a href="/employee" class="btn btn-info btn-sm">
            <i class="fas fa-angle-left"></i>
        </a>
        <a href="/employee/{{ $employee->id }}/edit" class="btn btn-warning btn-sm">
            <i class="fas fa-edit"></i> Edit
        </a>
        <form action="/employee/{{ $employee->id }}" method="POST" class="d-inline">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">
                <i class="fas fa-trash"></i> Delete
            </button>
        </form>
    </div>

    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong>{{ session('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6 border-end border-2">
            <div class="mb-3 row">
                <label class="col-sm-3 col-form-label fw-bolder">employee Name</label>
                <div class="col-sm-9">
                    <p class="fs-6 border-bottom px-2 py-1">{{ $employee->name }}</p>
                </div>
            @endsection
