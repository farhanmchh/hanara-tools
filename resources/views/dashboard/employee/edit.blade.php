@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Edit Employee Data</h1>
    </div>

    <a href="/employee" class="btn btn-danger btn-sm mb-3">
        <i class="fas fa-times-circle"></i> Cancel
    </a>

    <form action="/employee/{{ $employee->id }}" method="POST">
        @method('PATCH')
        @csrf
        <div class="row ">
            <div class="col-md-6">
                <div class="mb-3 row">
                    <label class="col-sm-3 col-form-label fw-bolder">Employee Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                            value="{{ $employee->name }}">
                    </div>
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 mb-3">
                    <div class="d-grid">
                        <button type="submit" class="btn btn-warning">
                            <i class="fas fa-paper-plane"></i> Update
                        </button>
                    </div>
                </div>
            </div>
    </form>
@endsection
