@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Category Project</h1>
    </div>

    <a href="/project_category/create" class="btn btn-primary mb-3">
        <i class="fas fa-plus"></i> Create a new Project Category
    </a>

    <div class="row">
        <div class="container my-2">
            <div class="my-4">
                <div class="row input">
                    <div class="col-md-9">
                        <input id="search-box" class="form-control col-lg-10 col-sm-12 fas fa-code-branch" placeholder="Search" name="text">
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong>{{ session('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong>{{ session('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="table-responsive">

        <table class="table table-striped table-sm text-center">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Project Category Name</th>
                    <th scope="col">From Project</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody id="category_table">
                @foreach ($project_category as $value)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->project->name}}</td>
                        <td>
                            <a href="/project_category/{{ $value->id }}" class="btn btn-info btn-sm"><i
                                    class="far fa-eye"></i></a>
                            <a href="/project_category/{{ $value->id }}/edit" class="btn btn-warning btn-sm"><i
                                    class="far fa-edit"></i></a>
                            <form action="/project_category/{{ $value->id }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Are you sure?')">
                                    <i class="fas fa-times-circle"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection


@section('script')
<script>
    $(document).ready(function(){
        $("#search-box").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#category_table tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
@endsection