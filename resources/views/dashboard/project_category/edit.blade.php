@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Edit Project Category</h1>
    </div>

    <a href="/employee" class="btn btn-danger btn-sm mb-3">
        <i class="fas fa-times-circle"></i> Cancel
    </a>

    <form action="/project_category/{{ $project_category->id }}" method="POST">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label for="name" class="col-sm-2 col-form-label fw-bolder">Name:</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" name="name"
                            class="form-control form-control-sm text-center @error('name') is-invalid @enderror"
                            value="{{ old('name') ? old('name') : $project_category->name }}" autocomplete="off">
                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label for="project_id" class="col-sm-2 col-form-label fw-bolder">Project:</label>
                    </div>
                    <div class="col-md-9">
                        <select name="project_id" id="to" class="form-select text-center @error('project_id') is-invalid @enderror">
                            <option></option>
                            @foreach ($project as $project)
                                @if ($project_category->project_id == $project->id)
                                    <option value="{{ $project->id }}" selected>{{ $project->name }}</option>
                                @else
                                    <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('project_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            <div class="row">
                <div class="col-md d-grid">
                    <button type="submit" class="btn btn-warning my-4">
                        <i class="fas fa-paper-plane"></i> Update
                    </button>
                </div>
            </div>
    </form>

@endsection
