<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>

  <p>Dear <b>{{ $quotation->customer->name }}.</b></p>
  <p>Here we attach the quotation {{ $quotation->subject }}.</p>
  <p>if there is anything you want to ask about the quotation, please contact us at the number {{ $company->phone }} or contact me at the number {{ $company->pic_phone }}</p>
  <br>
  <p>Best Regards</p>
  <p><b>{{ $company->name }}</b></p>
  
</body>
</html>