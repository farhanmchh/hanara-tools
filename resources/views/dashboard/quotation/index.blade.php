@extends('dashboard.layouts.main')

@section ('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Quotation</h1>
</div>

<a href="/quotation/create" class="btn btn-primary mb-3">
	<i class="fas fa-plus"></i> Create a new Quotation
</a>

@if (session('success'))
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<strong>{{ session('success') }}</strong>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
@endif

@if (session('error'))
	<div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
		<strong>{{ session('error') }}</strong>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	</div>
@endif

<div class="table-responsive">

	<table class="table table-striped table-sm text-center">
		<thead>
			<tr>
				<th scope="col">No</th>
				<th scope="col">Customers</th>
				<th scope="col">Date</th>
				<th scope="col">Status</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($quotations as $quotation)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $quotation->customer->name }}</td>
				<td>{{ $quotation->date }}</td>
				<td>{{ $quotation->status }}</td>
				<td>
					<a href="/quotation/{{ $quotation->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
					<a href="/print_quotation/{{ $quotation->id }}" class="btn btn-success btn-sm"><i class="fas fa-print"></i></a>
					{{-- @if ($quotation->status == 'not send') --}}
						<a href="/quotation/{{ $quotation->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
						<a href="/quotation/email/{{ $quotation->id }}" class="btn btn-primary btn-sm"><i class="fas fa-paper-plane"></i></a>
					{{-- @endif --}}
					<form action="/quotation/{{ $quotation->id }}" method="POST" class="d-inline">
						@method('delete')
						@csrf
						<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">
							<i class="fas fa-times-circle"></i>
						</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection