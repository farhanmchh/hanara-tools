@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Edit Quotation</h1>
</div>

<form action="/quotation/{{ $quotation->id }}" method="POST">
  @method('PUT')
  @csrf
  <div class="row">
    <div class="col-md-9">
      <a href="/quotation" class="btn btn-danger btn-sm mb-3">
        <i class="fas fa-times-circle"></i> Cancel
      </a>
    </div>
    <div class="col-md-3">
        <select name="status" id="status" class="form-select form-select-sm text-center">
          @if ($quotation->status == 'not send')
            <option value="not send" @if($quotation->status == 'not send') selected @endif>Not Send</option>
          @else
            <option value="waiting response" @if($quotation->status == 'waiting response') selected @endif>Waiting Response</option>
            <option value="follow up" @if($quotation->status == 'follow up') selected @endif>Follow Up</option>
            <option value="deal" @if($quotation->status == 'deal') selected @endif>Deal</option>
            <option value="not deal" @if($quotation->status == 'not deal') selected @endif>Not Deal</option>
          @endif
        </select>
        @error('customer_id')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
    </div>
  </div>

  <div class="row text-center">
    <div class="col-md-6">
      <div class="mb-3 row">
        <label for="to" class="col-sm-2 col-form-label fw-bolder fw-bolder">To :</label>
        <div class="col-sm-10">
          <select name="customer_id" id="to" class="form-select form-select-sm text-center @error('customer_id') is-invalid @enderror">
            <option></option>
            @foreach ($customers as $customer)
              @if ($quotation->customer_id == $customer->id)
                <option value="{{ $customer->id }}" selected>{{ $customer->name }}</option>
              @else
                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
              @endif
            @endforeach
          </select>
          @error('customer_id')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="mb-3 row">
        <label for="attn" class="col-sm-2 col-form-label fw-bolder">Attn :</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm text-center" id="attn" readonly>
        </div>
      </div>
      <div class="mb-3 row">
        <label for="subject" class="col-sm-2 col-form-label fw-bolder">Subject :</label>
        <div class="col-sm-10">
          <input type="text" name="subject" class="form-control form-control-sm text-center @error('subject') is-invalid @enderror" 
          value="{{ old('subject') ? old('subject') : $quotation->subject }}" autocomplete="off">
          @error('subject')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="mb-3 row">
        <label for="from" class="col-sm-2 col-form-label fw-bolder">From :</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm text-center" value="{{ $company->pic }}" readonly>
        </div>
      </div>
      <div class="mb-3 row">
        <label for="date" class="col-sm-2 col-form-label fw-bolder">Date :</label>
        <div class="col-sm-10">
          <input type="date" name="date" class="form-control form-control-sm text-center @error('date') is-invalid @enderror" style="height: 30px"
          value="{{ old('date') ? old('date') : $quotation->date }}">
          @error('date')
            <div class="invalid-feedback">
              {{ $message }}
            </div>
          @enderror
        </div>
      </div>
      <div class="mb-3 row">
        <label for="no" class="col-sm-2 col-form-label fw-bolder">No :</label>
        <div class="col-sm-10">
          <input type="text" name="code" class="form-control form-control-sm text-center" value="{{ $quotation->code }}" readonly>
        </div>
      </div>
    </div>
  </div>
  
  <h3 class="mt-3">Input Quotation</h3>

  <div class="table-responsive">
    <table class="table table-sm table-bordered text-center @error('item') border-danger @enderror">
      <thead class="bg-warning">
        <tr>
          <th scope="col">No</th>
          <th scope="col">Description</th>
          <th scope="col">Qty</th>
          <th scope="col">Freq</th>
          <th scope="col">Unit Price</th>
          <th scope="col">Amount</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      @php $i = 1 @endphp
      <tbody id="row-container">
        @foreach ($quotation->quotationItem as $quotationItem)
        <tr class="row-item">
          <td>{{ $loop->iteration }}</td>
          <td>
            <select name="item[]" class="item form-select form-select-sm text-center">
              @foreach ($items as $i)
                @if ($quotationItem->item_id == $i->id)
                <option value="{{ $quotationItem->item_id }}" selected>{{ $quotationItem->item->name }}</option>
                @else
                <option value="{{ $i->id }}">{{ $i->name }}</option>
                @endif
              @endforeach
            </select>
          </td>
          <td>
            <input type="text" name="quantity[]" class="quantity form-control form-control-sm text-center" value="{{ $quotationItem->quantity }}">
          </td>
          <td>
            <select name="frequency[]" class="form-select form-select-sm">
              <option value="onetime">One Time</option>
              <option value="monthly">Monthly</option>
              <option value="yearly">Yearly</option>
            </select>
          </td>
          <td>
            <input type="number" name="" class="unit-price form-control form-control-sm text-center" value="{{ $quotationItem->price }}" readonly>
          </td>
          <td>
            <input type="text" name="amount[]" class="amount form-control form-control-sm text-center" value="{{ $quotationItem->amount }}" readonly>
          </td>
          <td>
            <a href="javascript:void(0)" class="delete-row btn btn-danger btn-sm">
              <i class="fas fa-times-circle"></i>
            </a>
          </td>
        </tr>
        @endforeach
      </tbody>
      <tbody>
        <tr>
          <td>
            <button type="button" class="btn btn-outline-info btn-sm" id="add-row">
              <i class="fas fa-plus"></i>
            </button>
          </td>
          <td colspan="6"></td>
        </tr>
      </tbody>
      <tbody>
        <tr>
          <th colspan="5" class="h5">Total</th>
          <th colspan="2">
            <input type="text" name="total" class="total form-control form-control-sm text-center" value="{{ $quotation->total }}" readonly>
          </th>
        </tr>
        <tr>
          <th colspan="5" class="h5">Discount (%)</th>
          <th colspan="2">
            <input type="text" name="discount" class="discount form-control form-control-sm text-center" value="{{ $quotation->discount }}" autocomplete="off">
          </th>
        </tr>
        <tr>
          <th colspan="5" class="h5">Grand Total</th>
          <th colspan="2">
            <input type="text" name="grand_total" class="grand-total form-control form-control-sm text-center" value="{{ $quotation->grand_total }}" readonly>
          </th>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="row justify-content-center">
    <div class="col-md-6 text-center">
      <div class="mb-3">
        <label for="tnc_id" class="form-label fw-bolder">Choose Terms & Conditions</label>
        <select name="tnc_id" id="tnc_id" class="form-select @error('tnc_id') is-invalid @enderror text-center">
          <option></option>
          @foreach ($tncs as $tnc)
            @if ($quotation->tnc_id == $tnc->id)
              <option value="{{ $tnc->id }}" selected>{{ $tnc->name }}</option>
            @else
              <option value="{{ $tnc->id }}">{{ $tnc->name }}</option>
            @endif
          @endforeach
        </select>
        @error('tnc_id')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md d-grid">
      <button type="submit" class="btn btn-warning my-4">
        <i class="fas fa-paper-plane"></i> Update
      </button>
    </div>
  </div>
</form>
  
{{-- Add & Delete Row --}}
<script>
  $('#add-row').click(() => $('#row-container').append(showRow()))


  $(document).on('click', '.delete-row', function(){
    $('.total').val($('.total').val() - $(this).closest('.row-item').find('.amount').val())
    $('.grand-total').val($('.total').val() - ($('.total').val() * $('.discount').val() / 100))
    
    $(this).closest(".row-item").remove();

    $(".row-number").each(function(index){
      $(this).html(index + 2)
    })
  });
  
  function showRow() {
    return `<tr class="row-item">
              <td class="row-number">${$(".row-item").length + 1}</td>
              <td>
                <select name="item[]" class="item form-select form-select-sm text-center">
                  <option>- Select Item -</option>
                  @foreach ($items as $item)
                  <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endforeach
                </select>
              </td>
              <td>
                <input type="number" name="quantity[]" class="quantity form-control form-control-sm text-center" value="1">
              </td>
              <td>
                <select name="frequency[]" class="form-select form-select-sm">
                  <option value="onetime">One Time</option>
                  <option value="monthly">Monthly</option>
                  <option value="yearly">Yearly</option>
                </select>
              </td>
              <td>
                <input type="text" class="unit-price form-control form-control-sm text-center" readonly>
              </td>
              <td>
                <input type="text" name="amount[]" class="amount form-control form-control-sm text-center" value="0" readonly>
              </td>
              <td>
                <a href="javascript:void(0)" class="delete-row btn btn-danger btn-sm">
                  <i class="fas fa-times-circle"></i>
                </a>
              </td>
            </tr>`
  }
</script>

{{-- Count Price --}}
<script>
  $('.item').each(function() {
    $(document).on('change', '.item', function() {
      let eachItem = $(this);
      $.get(`/quotation/getPrice/${eachItem.val()}`, function(data) {
        let selector = eachItem.closest('.row-item');
        selector.find('.unit-price').val(data[0]['price']);
        selector.find('.amount').val(data[0]['price'] * selector.find('.quantity').val());
        
        countTotal();

        $(document).on('keyup', selector.find('.quantity'), function() {
          selector.find('.amount').val(data[0]['price'] * selector.find('.quantity').val());
          $('.total').val(selector.find('.amount').val() * selector.find('.quantity').val());

          countTotal();
        });
      });

    });

    $(document).on('keyup', '.quantity', function() {
      let eachItem = $(this).closest('.row-item');
      $.get(`/quotation/getPrice/${eachItem.find('.item').val()}`, function(data) {
        eachItem.find('.amount').val(data[0]['price'] * eachItem.find('.quantity').val());
        $('.total').val(eachItem.find('.amount').val() * eachItem.find('.quantity').val());

        countTotal();
      })
    });
  });
  
  function countTotal() {
    let arr = [];
    let mainArr = [];

    $('.amount').each(function() {
      let values = parseInt($(this).val())
      arr.push(values)
    });
    
    mainArr.push(arr)
    let total = mainArr[0].reduce((acc, curr) => acc + curr);

    $('.total').val(total)

    $('.grand-total').val(total - (total * $('.discount').val() / 100))
  }
</script>

{{-- Input Customer --}}
<script>
  const to = document.querySelector('#to');
  const attn = document.querySelector('#attn');

  fetch(`/quotation/getCustomer/${to.value}`)
    .then(response => response.json())
    .then(data => attn.value = data[0]['pic_name'])

  to.addEventListener('change', function() {
    fetch(`/quotation/getCustomer/${to.value}`)
      .then(response => response.json())
      .then(data => attn.value = data[0]['pic_name'])
  });

  // $(document).ready(function() {
  //   $('#to').on('change', function() {
  //     $.get(`/quotation/getCustomer/${$('#to').val()}`, function(data) {
  //       $('#attn').val(data[0]['pic_name'])
  //     });
  //   });
  // });

  // $(document).ready(function() {
  //   $.get(`/quotation/getCustomer/${$('#to').val()}`, function(data) {
  //     $('#attn').val(data[0]['pic_name'])
  //   });
  // });
</script>

@endsection