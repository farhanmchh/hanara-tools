@extends('dashboard.layouts.main')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Detail Quotation</h1>
</div>

<a href="/quotation" class="btn btn-info btn-sm">
  <i class="fas fa-angle-left"></i>
</a>
<a href="/quotation/email/{{ $quotation->id }}" class="btn btn-primary btn-sm">
  <i class="fas fa-envelope"></i> Send Email
</a>

{{-- <div class="table-responsive">
  <table class="table table-striped table-sm text-center">
    <thead>
      <tr>
        <th>To</th>
        <th>:</th>
        <th>{{ $quotation->customer->name }}</th> 
        <th>From</th>
        <th>:</th>
        <th>{{ $company->pic }}</th>
      </tr>
      <tr>
        <th>Attn</th>
        <th>:</th>
        <th>{{ $quotation->customer->pic_name }}</th>
        <th>Date</th>
        <th>:</th>
        <th>{{ $quotation->date }}</th>
      </tr>
      <tr>
        <th>Subject</th>
        <th>:</th>
        <th>{{ $quotation->subject }}</th>
        <th>No.</th>
        <th>:</th>
        <th>{{ $quotation->code }}</th>
      </tr>
    </thead>
  </table>
</div> --}}

<div class="mt-3">
  <p>Dear {{ $quotation->customer->name }}</p>
  <p>
    Referring to your Quotation request Regarding <b>{{ $quotation->subject }}</b>. We are please to submit our best price as follow:
  </p>
</div>
  
<div class="table-responsive">
  <table class="table table-sm table-bordered border-dark align-middle">
    <thead class="bg-warning text-center">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Description</th>
        <th scope="col">Qty</th>
        <th scope="col">Freq</th>
        <th scope="col">Unit Price</th>
        <th scope="col">Amount</th>
      </tr>
    </thead>
    <tbody id="row-container">
      @foreach ($quotation->quotationItem as $quotationItem)
      <tr class="row-item text-middle">
        <td class="text-center">{{ $loop->iteration }}</td>
        <td>
          <b>{{ $quotationItem->item->name }}</b>
          {!! htmlspecialchars_decode($quotationItem->item->description) !!}
        </td>
        <td class="text-center">{{ $quotationItem->quantity }}</td>
        <td class="text-center">{{ $quotationItem->frequency }}</td>
        <td class="text-center">{{ formatPrice($quotationItem->item->price) }}</td>
        <td class="text-center">{{ formatPrice($quotationItem->amount) }}</td>
      </tr>
      @endforeach
    </tbody>
    <tbody class="text-center">
      <tr>
        <th colspan="5" class="h5">Total</th>
        <th>{{ formatPrice($quotation->total) }}</th>
      </tr>
      <tr>
        <th colspan="5" class="h5">Discount</th>
        <th>{{ $quotation->discount }}%</th>
      </tr>
      <tr>
        <th colspan="5" class="h5">Grand Total</th>
        <th>{{ formatPrice($quotation->grand_total) }}</th>
      </tr>
    </tbody>
  </table>
</div>

<div class="mb-5">
  <p><b>Terms & Conditions</b></p>
  {!! $quotation->tnc->body !!}
</div>

@endsection