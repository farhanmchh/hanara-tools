@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Create Terms & Conditions</h1>
</div>

<a href="/tnc/create" class="btn btn-primary mb-3">
  <i class="fas fa-plus"></i> Create
</a>

@if (session('success'))
  <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('success') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

@if (session('error'))
  <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
    <strong>{{ session('error') }}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

<div class="table-responsive">
  <table class="table table-sm text-center">
    <thead>
      <tr>
        <th>No</th>
        <th>Body</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tncs as $tnc)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $tnc->name }}</td>
          <td>
            <a href="/tnc/{{ $tnc->id }}" class="btn btn-info btn-sm"><i class="far fa-eye"></i></a>
            <a href="/tnc/{{ $tnc->id }}/edit" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
            <form action="/tnc/{{ $tnc->id }}" method="POST" class="d-inline">
              @method('delete')
              @csrf
              <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this tnc?')"><i class="fas fa-trash"></i></button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection