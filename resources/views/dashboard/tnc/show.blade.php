@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Detail Terms & Conditions</h1>
</div>

<a href="/tnc" class="btn btn-secondary btn-sm my-3">
  <i class="fas fa-arrow-left"></i> Back
</a>

<div class="row">
  <div class="col-md-6">
    <h4>{{ $tnc->name }}</h4>
    <h6>{!! $tnc->body !!}</h6>
  </div>
</div>

@endsection