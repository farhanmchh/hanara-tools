@extends('dashboard.layouts.main')

@section('content')
    
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2 text-center">Terms & Conditions</h1>
</div>

<a href="/tnc" class="btn btn-secondary btn-sm my-3">
  <i class="fas fa-arrow-left"></i> Back
</a>

<form action="/tnc" method="POST">
  @csrf
  <div class="row">
    <div class="col-md-7">
      <div class="mb-3">
        <label for="name" class="form-label fw-bolder">* Name</label>
        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
        @error('name')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="mb-3">
        <label for="body" class="form-label fw-bolder">* Body</label>
        @error('body')
          <p class="text-danger">{{ $message }}</p>
        @enderror
        <input type="hidden" name="body" id="body" value="{{ old('body') }}">
        <trix-editor input="body"></trix-editor>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-7 d-grid">
      <button type="submit" class="btn btn-primary">Create</button>
    </div>
  </div>
</form>

@endsection