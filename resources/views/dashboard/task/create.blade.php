@extends('dashboard.layouts.main')

@section('style')
    <style>
        .hide{
            display: none;
        }
    </style>
@endsection

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2>Create a New </h2>
    </div>

    <a href="/employee" class="btn btn-danger btn-sm mb-3">
        <i class="fas fa-times-circle"></i> Cancel
    </a>

    <section id="input-form">
        <form action="/task" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="name" class="col-sm-2 col-form-label fw-bolder">Name:</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="name" class="form-control form-control-sm text-center @error('name') is-invalid @enderror" value="{{ old('name') }}" autocomplete="off">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-3">
                            <label for="project_id" class="col-sm-2 col-form-label fw-bolder">Project:</label>
                        </div>
                        <div class="col-md-9">
                            <select id="project-option" name="projects_id" class="form-select text-center @error('projects_id') is-invalid @enderror">
                                <option disabled selected> Choose Project </option>
                                @foreach ($project as $project)
                                    @if (old('projects_id') == $project->id)
                                        <option value="{{ $project->id }}" selected>{{ $project->name }}</option>
                                    @else
                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('project_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div id="category-row" class="row hide mt-3">
                        <div class="col-md-3">
                            <label for="project_category_id" class="col-sm-2 col-form-label fw-bolder">Category:</label>
                        </div>
                        <div class="col-md-9">
                            <select id="category-option" name="project_category_id" class="form-select text-center @error('project_category_id') is-invalid @enderror">
                                <option disabled selected> Choose Category </option>
                            </select>
                            @error('project_category_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-3">
                            <label for="employee_id" class="col-sm-2 col-form-label fw-bolder">Assignee:</label>
                        </div>
                        <div class="col-md-9">
                            <select name="employee_id" id="to" class="form-select text-center @error('employee_id') is-invalid @enderror">
                                <option selected disabled> Choose Employee </option>
                                @foreach ($employee as $employee)
                                    @if (old('employee_id') == $employee->id)
                                        <option value="{{ $employee->id }}" selected>{{ $employee->name }}</option>
                                    @else
                                        <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('employee_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-3">
                            <label for="task_status" class="col-sm-2 col-form-label fw-bolder">Status:</label>
                        </div>
                        <div class="col-md-9">
                            <select name="task_status" id="to" class="form-select text-center @error('task_status_id') is-invalid @enderror">
                                <option selected disabled> Choose Status </option>
                                <option>Plan & To Do</option>
                                <option>On Hold</option>
                                <option>Working</option>
                                <option>Done</option>
                                <option>QA</option>
                            </select>
                            @error('task_status')
                                <div class="invalid-feedback">  
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="deadline" class="col-sm-2 col-form-label fw-bolder">Deadline:</label>
                        </div>
                        <div class="col-md-9">
                            <input type="date" name="deadline" class="form-control form-control-sm text-center @error('deadline') is-invalid @enderror" value="{{ old('deadline') }}">
                            @error('deadline')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-3">
                            <label for="description" class="col-sm-2 col-form-label fw-bolder">Description:</label>
                        </div>
                        <div class="col-md-9">
                            <textarea style="height: 20vh;" name="description" class="form-control form-control-sm @error('description') is-invalid @enderror" value="{{ old('name') }}" autocomplete="off"></textarea>
                            @error('description')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" style="float: right;" class="form-control btn btn-primary my-4">
                        <i class="fas fa-plus"></i> Create
                    </button>
                </div>
            </div>
        </form>
    </section>


@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11" ></script>  
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $( "#project-option" ).change(function() {
            var value = $(this).val();
            getCategory(value);
        });

        function getCategory(category_id){
            $.ajax({
		        type: "POST",
		        url: '{{URL::to('/category-project')}}',
		        data: {
		            id: category_id
		        },
		        dataType: "json",
		        success: function (data) {
                    // console.log(data);

                    var status = data.status;
                    if(status == 'error'){
                        var msg = data.messages
                        Swal.fire(
                            'Error',
                            msg,
                            'error'
                        )
                    }else{
                        var result = data.data;
                        $('#category-row').removeClass('hide');

                        // $('#mySelect').append($('<option>', {
                        //     value: 1,
                        //     text: 'My option'
                        // }));
                        $('.category-select').remove();
                        $.each(result, function (key, value) {
                            $('#category-option').append($('<option>', {
                                class: 'category-select',
                                value: value.id,
                                text: value.name
                            }));
                        })
                        // console.log(result)
                    }
                }
            })
        }
    </script>
@endsection

