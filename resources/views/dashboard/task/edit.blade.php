@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Edit task</h1>
    </div>

    <a href="/employee" class="btn btn-danger btn-sm mb-3">
        <i class="fas fa-times-circle"></i> Cancel
    </a>

    <form action="/task/{{ $task->id }}" method="POST">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label for="name" class="col-sm-2 col-form-label fw-bolder">Name:</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" name="name"
                            class="form-control form-control-sm text-center @error('name') is-invalid @enderror"
                            value="{{ old('name') ? old('name') : $task->name }}" autocomplete="off">
                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label for="projects_id" class="col-sm-2 col-form-label fw-bolder">Project:</label>
                    </div>
                    <div class="col-md-9">
                        <select id="project-option" name="projects_id" class="form-select text-center @error('projects_id') is-invalid @enderror">
                            <option disabled selected>{{ $task->project->name }}</option>
                            @foreach ($project as $project)
                                @if ($task->project_id == $project->id)
                                    <option value="{{ $project->id }}" selected>{{ $project->name }}</option>
                                @else
                                    <option value="{{ $project->id }}">{{ $project->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('projects_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div id="category-row" class="row mt-3">
                    <div class="col-md-3">
                        <label for="project_category_id" class="col-sm-2 col-form-label fw-bolder">Category:</label>
                    </div>
                    <div class="col-md-9">
                        <select id="category-option" name="project_category_id" class="form-select text-center @error('project_category_id') is-invalid @enderror">
                        </select>
                        @error('project_category_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label for="employee_id" class="col-sm-2 col-form-label fw-bolder">Assignee:</label>
                    </div>
                    <div class="col-md-9">
                        <select name="employee_id" id="to" class="form-select text-center @error('employee_id') is-invalid @enderror">
                            <option></option>
                            @foreach ($employee as $employee)
                                @if ($task->employee_id == $employee->id)
                                    <option value="{{ $employee->id }}" selected>{{ $employee->name }}</option>
                                @else
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('employee_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label for="task_status" class="col-sm-2 col-form-label fw-bolder">Status:</label>
                    </div>
                    <div class="col-md-9">
                        <select name="task_status" class="form-select text-center @error('task_status') is-invalid @enderror">
                            <option value="{{ $task->task_status }}" selected>{{ $task->task_status }}</option>
                            <option value="Plan & To Do" @if($task->task_status == 'Plan & To Do') selected @endif>Plan & To Do</option>
                            <option value="On Hold" @if($task->task_status == 'On Hold') selected @endif>On Hold</option>
                            <option value="Working" @if($task->task_status == 'Working') selected @endif>Working</option>
                            <option value="Done" @if($task->task_status == 'Done') selected @endif>Done</option>
                            <option value="QA" @if($task->task_status == 'QA') selected @endif>QA</option>
                          </select>
                        @error('task_status')
                            <div class="invalid-feedback">  
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <label for="deadline" class="col-sm-2 col-form-label fw-bolder">Deadline:</label>
                    </div>
                    <div class="col-md-9">
                        <input type="date" name="deadline" class="form-control form-control-sm text-center @error('deadline') is-invalid @enderror" value="{{ old('deadline') ? old('deadline') : $task->deadline }}">
                            @error('deadline')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-3">
                        <label for="description" class="col-sm-2 col-form-label fw-bolder">Description:</label>
                    </div>
                    <div class="col-md-9">
                        <input style="height: 20vh;" name="description" class="form-control form-control-sm @error('description') is-invalid @enderror" value="{{ old('description') ? old('description') : $task->description }}" autocomplete="off">
                        @error('description')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" style="float: right;" class="form-control btn btn-warning my-4">
                    <i class="fas fa-paper-plane"></i> Update
                </button>
            </div>
        </div>
    </form>


@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11" ></script>  
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $( "#project-option" ).change(function() {
            var value = $(this).val();
            getCategory(value);
        });

        function getCategory(category_id){
            $.ajax({
		        type: "POST",
		        url: '{{URL::to('/category-project')}}',
		        data: {
		            id: category_id
		        },
		        dataType: "json",
		        success: function (data) {
                    // console.log(data);

                    var status = data.status;
                    if(status == 'error'){
                        var msg = data.messages
                        Swal.fire(
                            'Error',
                             msg,
                            'error'
                        )
                    }else{
                        var result = data.data;
                        $('#category-row').removeClass('hide');

                        // $('#mySelect').append($('<option>', {
                        //     value: 1,
                        //     text: 'My option'
                        // }));
                        $('.category-select').remove();
                        $.each(result, function (key, value) {
                             $('#category-option').append($('<option>', {
                                class: 'category-select',
                                value: value.id,
                                text: value.name
                            }));
                        })
                    }
                }
            })
        }
        $(document).ready(function(){
            getCategory({{$task->project_id}})
            
        })
    </script>
@endsection

{{-- var status = data.status;
if(status == 'error'){
    var msg = data.messages
    Swal.fire(
        'Error',
        msg,
        'error'
    )
}else{
    var result = data.data;
    $('#category-row').removeClass('hide');

    // $('#mySelect').append($('<option>', {
    //     value: 1,
    //     text: 'My option'
    // }));
    $('.category-select').remove();
    $.each(result, function (key, value) {
        $('#category-option').append($('<option>', {
            class: 'category-select',
            value: value.id,
            text: value.name
        }));
    }) --}}