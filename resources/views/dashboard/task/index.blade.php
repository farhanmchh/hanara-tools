@extends('dashboard.layouts.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2 text-center">Task</h1>
    </div>

    <a href="/task/create" class="btn btn-primary mb-3">
        <i class="fas fa-plus"></i> Create a new task
    </a>

    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong>{{ session('success') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong>{{ session('error') }}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="table-responsive">

        <table class="table table-striped table-sm text-center">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Task Name</th>
                    <th scope="col">PIC</th>
                    <th scope="col">Project</th>
                    <th scope="col">Category Project</th>
                    <th scope="col">Status</th>
                    <th scope="col">Deadline</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $value)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->employee->name }}</td>
                        <td>{{ $value->project->name }}</td>
                        <td>{{ $value->project_category->name }}</td>
                        <td>{{ $value->task_status }}</td>
                        <td>{{ $value->deadline }}</td>
                        <td>
                            <a href="/task/{{ $value->id }}" class="btn btn-info btn-sm"><i
                                    class="far fa-eye"></i></a>
                            <a href="/task/{{ $value->id }}/edit" class="btn btn-warning btn-sm"><i
                                    class="far fa-edit"></i></a>
                            <form action="/task/{{ $value->id }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Are you sure?')">
                                    <i class="fas fa-times-circle"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
