<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF MOM</title>
<style>
   @page {
        margin: 0cm 0cm;
    }

    * {
        font-family: Arial, Helvetica, sans-serif;
    }

    body {
        margin-top: 3cm;
        margin-left: 2cm;
        margin-right: 2cm;
        margin-bottom: 2cm;
    }

    header {
        position: fixed;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        height: 0cm;
        float: right;
    }   

    footer { 
        position: fixed; 
        bottom: 0cm; 
        left: 0cm; 
        right: 0cm;
        height: 2cm;
    }

    footer .page:after { content: counter(page, decimal); }

    .table1{
        text-align: left;
    }
    .a1{
        text-align: left;
    }
    .text-center{
        text-align: center;
    }
</style>

</head>
<body>

  {{-- Footer Page Number --}}
  <footer>  
    <p class="page" style="font-family: 'times new roman'; font-size: 100%; text-align: center;"></p>
  </footer>
  {{-- End Footer --}}

  {{-- HEADER --}}
  <header><img src="img/hanara-logo.png" alt="logo"></header>
  {{-- END HEADER --}}
  
  <main>
<h2 style="margin-top: -30px">Minutes Of Meeting</h2>

{{-- SUMMARY --}}
        <h3>Summary</h3>
        <table border="1" cellspacing="0" cellpadding="10" width="100%">
            <tr>
                <th align="left" colspan="6"><i>Meeting Subject :</i></th>
            </tr>        
            <tr>
                <th align="left" colspan="6" style="border-top: hidden"><i>{{ $mom->subject }}</i></th>
            </tr>        
            <tr>
                <th class="a1"><i>Date</i></th>
                <td class="a1">{{ $mom->date }}</td>
                <th class="a1"><i>From</i></th>
                <td class="a1">{{ $mom->from }}</td>
                <th class="a1"><i>To</i></th>
                <td class="a1">{{ $mom->to }}</td>
            </tr>        
            <tr class="tr1">
                <th class="a1"><i>Location</i></th>
                <td class="a1">{{ $mom->location }}</td>
                <th class="a1"><i>Note Taker</i></th>
                <td class="a1">{{ $mom->note_taker }}</td>
                <th class="a1"><i>Duration</i></th>
                <td class="a1">{{ $mom->duration }}</td>
            </tr>        
        </table>
{{-- END SUMMARY --}}
        
{{-- AGENDA --}}
    <h3>Agenda</h3>
    <table border="1" cellspacing="0" cellpadding="10" width="100%">
        <tr>
            <th width="30px"><i>No.</i></th>
            <th><i>Topic</i></th>
        </tr>
        @foreach ($mom->momTopic as $momTopic)
        <tr>
            <td align="center">{{ $loop->iteration }}</td>
            <td>{{ $momTopic->topic }}</td>
        </tr>
    @endforeach
    </table>
{{-- END AGENDA --}}

{{-- ATTENDEE LIST --}}
    <h3>Attendee List</h3>
    <table border="1" cellspacing="0" cellpadding="10" width="100%">
        <tr>
            <th><i>No.</i></th>
            <th><i>Name</i></th>
            <th><i>Company</i></th>
            <th><i>Position</i></th>
            <th><i>Email</i></th>
          </tr>
          @foreach ($mom->momAttendee as $momAttendee)
            <tr>
              <td align="center">{{ $loop->iteration }}</td>
              <td>{{ $momAttendee->name }}</td>
              <td>{{ $momAttendee->company }}</td>
              <td>{{ $momAttendee->position }}</td>
              <td>{{ $momAttendee->email }}</td>
            </tr>
          @endforeach
    </table>
{{-- END ATTENDEE LIST --}}

{{-- ISSUE AND ACTION PLAN --}}
    <h3>Issue and Action Plan</h3>
    <table border="1" cellspacing="0" cellpadding="10" width="100%">
        <tr>
            <th><i>No.</i></th>
            <th><i>Issue Description</i></th>
            <th><i>Type</i></th>
            <th><i>Assigned To</i></th>
            <th><i>Due Date</i></th>
            <th><i>Resolution/Status</i></th>
            <th><i>Date Resolved</i></th>
          </tr>
          @foreach ($mom->momIssue as $momIssue)
            <tr>
              <td  class="text-center">{{ $loop->iteration }}</td>
              <td>{!! $momIssue->description !!}</td>
              <td class="text-center">{{ $momIssue->type }}</td>
              <td class="text-center">{{ $momIssue->assigned }}</td>
              <td class="text-center">{{ $momIssue->due_date }}</td>
              <td class="text-center">{{ $momIssue->status }}</td>
              <td class="text-center">{{ $momIssue->date_resolved }}</td>
            </tr>
          @endforeach
    </table>
{{-- END ISSUE AND ACTION PLAN --}}

{{-- TNC --}}
    <h6><i>Terms & Conditions: </i></h6>
    <p>{!! $mom->tnc !!}</p>
{{-- END TNC --}}
<br>
{{-- PREPARED BY --}}
  <table border="1" cellspacing="0" cellpadding="10" style="width: 100%;">
    <tr>
      <th width="20%" align="left"><i>Prepared By</i></th>
      <td width="50%">{{ $mom->note_taker }}</td>
      <td rowspan="2" width="30%"></td>
    </tr>
    <tr>
      <th align="left"><i>Date</i></th>
      <td>{{ $mom->date }}</td>
    </tr>
  </table>
{{-- END PREPARED BY --}}

{{-- VERIFIED BY --}}
  <table border="1" cellspacing="0" cellpadding="10" style="margin-top: 20px; width: 100%;">
    <tr>
      <th width="20%" align="left"><i>Verified By</i></th>
      <td width="50%">{{ $mom->verified_by }}</td>
      <td width="30%" rowspan="2"></td>
    </tr>
    <tr>
      <th align="left"><i>Date</i></th>
      <td>{{ $mom->verified_date }}</td>
    </tr>
  </table>
{{-- END VERIFIED BY --}}
  </main>
</body>
</html>