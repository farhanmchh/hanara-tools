<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDF Quotation</title>
  <style>
    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 50px;}

    body {
        font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
      }
      table {
        text-align: center;
      }
      .thead1 {
        background-color: #ffc955;
      }
      .header{
      padding: 20px;
      height: 10%;
    }
      
      .logo{

        /* background-color: orange; */
        height: 10.5%;
        width: 15.5%;
      }
      
      .company-profile {
        position: absolute;
        top: 2%;
        left: 21.5%;
        height: 10.5%;
        width: 82%;
        text-align: center;
      }

      .td1{
        padding: 5.7px;
      }
  </style>
</head>
<body>
  <footer>  
    <div class="header-text" style="line-height: 0.1;">
      <p style="font-weight: bolder; font-family: 'times new roman'; font-size: 100%; text-align: center;">{{ $company->name }}</p>
      <p style="font-family: 'times new roman'; font-size: 75%; text-align: center;">{{ $company->address }}</p>
      <p style="font-family: 'times new roman'; font-size: 75%; text-align: center;">
        Email : {{ $company->email }} | Website : {{ $company->website }} | Phone : {{ $company->phone }}</p>
    </div>
  </footer>
  <main>
        {{-- HEADER --}}
        <div class="header">
          <div class="logo"> 
            <img src="img/hanara-logo.png" alt="logo">
          </div>
          <div class="company-profile">
            <p style="font-size: 200%; margin: 0 0 -10px 0;"><b>{{ strtoupper($company->name) }}</b></p>
            <p>{{ $company->address }}</p>
            <p style="line-height: 0.1;">Telp: {{ $company->phone }}, Email : {{ $company->email }}</p>
          </div>
        </div>
        {{-- END HEADER --}}
      
      <hr>
      
      {{-- MAIL DETAILS --}}
        <div>
          <table class="font-arial mail-details table1">
            <tr style="background-color: rgb(250, 192, 144);">
                <td class="td1" width="39">To</td>
                <td class="td1" width="2.5">:</td>
                <td class="td1" width="190">{{ $quotation->customer->name }}</td>
                <td class="td1" width="39">From</td>
                <td class="td1" width="2.5">:</td>
                <td class="td1" width="190">{{ $company->pic }}</td>
            </tr>
            <tr style="background-color: rgb(250, 192, 144);">
                <td class="td1" width="39">Attn</td>
                <td class="td1" width="2.5">:</td>
                <td class="td1" width="190">{{ $quotation->customer->pic_name }}</td>
                <td class="td1" width="39">Date</td>
                <td class="td1" width="2.5">:</td>
                <td class="td1" width="190">{{ $quotation->date }}</td>
            </tr>
            <tr style="background-color: rgb(250, 192, 144);">
                <td class="td1" width="39">Subject</td>
                <td class="td1" width="2.5">:</td>
                <td class="td1" width="190">{{ $quotation->subject }}</td>
                <td class="td1" width="39">No.</td>
                <td class="td1" width="2.5">:</td>
                <td class="td1" width="190">{{ $quotation->code }}</td>
            </tr>   
          </table>
        </div>
        <br>
      {{-- END MAIL DETAILS --}}
      
      {{-- SUBJECT --}}
        <p>Dear {{ $quotation->customer->name }},</p>  
        <p>
          Referring to your Quotation request Regarding <b>{{ $quotation->subject }}</b>. We are please to submit our best price as follow:
        </p>
        <br>
          <div class="table-responsive"> 
          <table class="table_item" border="1" width="100%" cellspacing="0" cellpadding="10">
            <thead style="background-color: rgb(250, 192, 144)">
              <tr>
                <th>No.</th>
                <th>Description</th>
                <th>Qty</th>
                <th>Freq</th>
                <th>Unit Price</th>
                <th>Amount</th>
              </tr>
            </thead>
      
            <tbody>
              @foreach ($quotation->quotationItem as $quotationItem)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td align="left">
                  <p><b>{{ $quotationItem->item->name }}</b></p>
                  {!! htmlspecialchars_decode($quotationItem->item->description) !!}
                </td>
                <td>{{ $quotationItem->quantity }}</td>
                <td>{{ $quotationItem->frequency }}</td>
                <td>{{ formatPrice($quotationItem->price) }},-</td>
                <td>{{ formatPrice($quotationItem->amount) }},-</td>
              </tr>
              @endforeach
              
              
              <tr>
                <td colspan="5" class="count">Total</td>
                <td>{{ formatPrice($quotation->total) }},-</td>
              </tr>
      
              <tr>
                <td colspan="5" class="count">Discount</td>
                <td>{{ $quotation->discount ?? '-' }}</td>
              </tr>
              <tr>
                <td colspan="5" class="count">Grand Total</td>
                <td>{{ formatPrice($quotation->grand_total) }},-</td>
              </tr>
            </tbody>
          </table>
          </div>
      {{-- END SUBJECT --}}
      
      {{-- TNC --}}
          <div style="display: inline;">
          <p><b>Terms & Conditions</b></p>
         {!! $quotation->tnc->body !!}
          </div>
          <br>
      {{-- END TNC --}}
        
          <p style="margin-bottom: 100px;">Best Regards,</p>
          <p>
            {{ $company->pic }}
            <br>
            {{ $company->role }}
            <br>
            PT. {{ $company->name }}
          </p>
          </div>
        </div>
  </main>
</body>
</html>