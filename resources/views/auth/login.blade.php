<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    
    <link href="/css/signin.css" rel="stylesheet">
    
    <title>Login</title>
  </head>
  <body class="bg-white">
    <div class="container">
      <div class="row justify-content-center mb-5">
        <div class="col-md-5 mb-5">
          <main class="form-signin mb-5">
            
            <div class="col-md text-center mb-2">
              <img src="/img/hanara-logo.png" alt="hanara-logo">
            </div>
            
            <h1 class="h3 mb-4 text-center">Please Login</h1>
            
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
              <strong>{{ session('success') }}</strong>
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
              <strong>{{ session('error') }}</strong>
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            
            <form action="/" method="POST">
              @csrf  
              <div class="mb-3">
                <label for="email" class="form-label fw-bolder">Email address</label>
                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" autofocus>
                @error('email')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
              </div>
              
              <div class="mb-3">
                <label for="password" class="form-label fw-bolder">Password</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                @error('password')
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
                @enderror
              </div>
              <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
            </form>
          </main>
        </div>
      </div>
    </div>
    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

  </body>
</html>